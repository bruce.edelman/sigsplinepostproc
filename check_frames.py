import numpy as np
import matplotlib.pyplot as plt
from pycbc.frame import read_frame
from scipy.signal import welch


def transform(fs):
    outfs = fs.copy()
    return np.square(2 * np.abs(outfs) / np.sqrt(outfs.sample_frequencies))


start_time = 1126259462
end_time = 1126259494
seglen = 256
signal_noise = read_frame(
    f"/media/bedelman/SSD2/projects/sigsplinepostproc/scratch/flow_10/IMRPhenomD/H-H1_ModGR_f_rolloff_FD-0-{seglen}.gwf",
    "H1_modGR",
)  # , start_time=1, duration=31)
pure_sign = read_frame(
    f"/media/bedelman/SSD2/projects/sigsplinepostproc/scratch/flow_10/IMRPhenomD/H-H1_PureGR_NSBH_FD-0-{seglen}.gwf",
    "H1_modGR",
)
signal_only = read_frame(
    f"/media/bedelman/SSD2/projects/sigsplinepostproc/scratch/flow_10/IMRPhenomD/H-H1_ModGR_f_rolloff_SignalOnly_FD-0-{seglen}.gwf",
    "H1_modGR",
)
pure_sig = read_frame(
    f"/media/bedelman/SSD2/projects/sigsplinepostproc/scratch/flow_10/IMRPhenomD/H-H1_PureGR_NSBH_SignalOnly_FD-0-{seglen}.gwf",
    "H1_modGR",
)
noise_only = read_frame(
    f"/media/bedelman/SSD2/projects/sigsplinepostproc/scratch/flow_10/IMRPhenomD/H-H1_PureGR_NSBH_NoiseOnly_FD-0-{seglen}.gwf",
    "H1_modGR",
)  # , start_time=start_time, end_time=end_time)


fd_signal_noise = signal_noise.to_frequencyseries()
fd_signal_only = signal_only.to_frequencyseries()
fd_noise_only = noise_only.to_frequencyseries()
fd_pure = pure_sig.to_frequencyseries()

sample_rate = 4096.0 * 2
fd, data_psd = welch(signal_noise, fs=sample_rate, nperseg=1024 * 2)
fd2, data_psd2 = welch(pure_sign, fs=sample_rate, nperseg=1024 * 2)
fn, noise_psd = welch(noise_only, fs=sample_rate, nperseg=1024 * 2)

fig, axs = plt.subplots(nrows=2, ncols=3, figsize=(16, 10))
axs[0, 0].loglog(fd, data_psd ** (1 / 2.0), label="Signal+Noise", alpha=0.5)
axs[0, 0].loglog(fd2, data_psd2 ** (1 / 2.0), label="Signal+Noise_pure", alpha=0.5)
axs[0, 0].loglog(fd_signal_only.sample_frequencies, np.abs(fd_signal_only), label="Signal-Mod", alpha=0.5)
axs[0, 0].loglog(fd_pure.sample_frequencies, np.abs(fd_pure), label="Signal-Pure", alpha=0.5)
axs[0, 1].loglog(fd_signal_only.sample_frequencies, np.abs(fd_signal_only), label="Signal-Mod", alpha=0.5)
axs[0, 1].loglog(fd_pure.sample_frequencies, np.abs(fd_pure), label="Signal-Pure", alpha=0.5)
axs[0, 2].loglog(fn, noise_psd ** (1 / 2.0), label="Noise", alpha=0.5)
axs[0, 2].legend()
axs[0, 1].legend()
axs[0, 0].legend()
axs[0, 2].set_xlim(10.0, 2048.0 * 2)
axs[0, 1].set_xlim(10.0, 2048.0 * 2)
axs[0, 0].set_xlim(10.0, 2048.0 * 2)
axs[0, 2].set_ylim(1e-27, 5e-22)
axs[0, 1].set_ylim(1e-27, 5e-22)
axs[0, 0].set_ylim(1e-27, 5e-22)
axs[1, 0].plot(signal_noise.sample_times, signal_noise, label="Signal+Noise", alpha=0.5)
axs[1, 0].plot(pure_sign.sample_times, pure_sign, label="Signal+Noise_pure", alpha=0.5)
axs[1, 0].plot(signal_only.sample_times, signal_only, label="Mod-Signal", alpha=0.5)
axs[1, 0].plot(pure_sig.sample_times, pure_sig, label="Pure-Signal", alpha=0.5)
axs[1, 1].plot(signal_only.sample_times, signal_only, label="Mod-Signal", alpha=0.5)
axs[1, 1].plot(pure_sig.sample_times, pure_sig, label="Pure-Signal", alpha=0.5)
axs[1, 2].plot(noise_only.sample_times, noise_only, label="Noise", alpha=0.5)
axs[1, 2].legend()
axs[1, 1].legend()
axs[1, 0].legend()
plt.show()

plt.loglog(fd_pure.sample_frequencies, np.abs(fd_pure), label="Signal-Pure")
plt.loglog(fd_signal_only.sample_frequencies, np.abs(fd_signal_only), label="Signal-Mod")
plt.xlim(5.0, 2100.0 * 2)
plt.legend()
plt.ylim(1e-27, 5e-22)
plt.xlabel("Frequency (Hz)")
plt.ylabel("Strain Amp")
plt.savefig("nsbh_amp.png")
plt.show()
