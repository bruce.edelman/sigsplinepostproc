## Installation

    git clone https://git.ligo.org/bruce.edelman/sigsplinepostproc.git
    
In cloned source directory:

    pip install .
    
or if you want the local installation to track changes in source files 

    pip install -e .
    
    

