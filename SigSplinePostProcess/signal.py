from pycbc.frame import read_frame
import numpy as np
import os, h5py
from matplotlib import pyplot as plt
from pycbc.waveform.utils import phase_from_polarizations
from pycbc.types import FrequencySeries
from pycbc.waveform.generator import FDomainCBCGenerator, FDomainDetFrameGenerator


DATADUMP_SUFFIXS = ["-freqData.dat", "-freqDataWithInjection.dat", "-timeData.dat", "-timeDataWithInjection.dat"]
PYCBC_WAVEFORM_PARAMS = ["mass1", "mass2", "distance", "spin1z", "spin2z", "tc", "ra", "dec", "polarization"]
DEFAULT_INJECTED_PARAMS = {
    "mass1": 36,
    "mass2": 29,
    "distance": 450,
    "spin1z": 0.0,
    "spin2z": 0.0,
    "tc": 1126259462,
    "ra": 1.37,
    "dec": -1.26,
    "polarization": 2.76,
}
DEFAULT_FARR_PARAMS = {"dphase": 1.047, "fz": 102.0, "deltaf": 1.0, "damp": 0.1, "dfz": 0.0, "refreq": 100.0}


class Signal(object):
    def __init__(self, file, ifo, **kwargs):
        self.file = file
        args = kwargs
        file_tpye = file.split(".")[-1]
        if file_tpye == "gwf":
            if kwargs["end_time"] is None and kwargs["duration"] is None:
                raise LookupError("Provide either end_time or duration")
            channel = kwargs["channel"]
            args.pop("channel")
            ts = read_frame(self.file, channel, **args)
            fs = ts.to_frequencyseries()
            self.amp_fd, self.amp_td = np.abs(fs.data), np.abs(ts.data)
            self.times, self.freqs = ts.sample_times, fs.sample_frequencies
            self.phase_fd, self.phase_td = self.phase_from_frame(ts, fs)
            self.hpf, self.hcf, self.hpt, self.hct = self.polarizations_from_frame(ts, fs)
        elif file_tpye == "dat":
            self.freqs, self.hpf, self.hcf = np.genfromtxt(self.file, unpack=True)
            fs = self.hpf + 1j * self.hcf
            fs = FrequencySeries(fs, delta_f=self.freqs[1] - self.freqs[0])
            ts = fs.to_timeseries()
            self.times = ts.sample_times
            self.amp_td = np.abs(ts.data)
            self.amp_fd = np.abs(fs.data)
            self.hct = ts.copy()
            self.hpt = ts.copy()
            self.hct.data = np.imag(ts.data)
            self.hpt.data = np.real(ts.data)
            self.phase_fd = np.unwrap(np.angle(fs))
            self.phase_td = phase_from_polarizations(self.hpt, self.hct)
        elif file_tpye == "npy":
            fs = load_frequencyseries(file)
            self.amp_fd = np.abs(fs.data)
            self.freqs = fs.sample_frequencies
            self.phase_fd = np.unwrap(np.angle(fs))
        else:
            raise ValueError(f"file type .{file_tpye} not understood for signal plotting")

        self.ifo = ifo

    @staticmethod
    def phase_from_frame(ts, fs):
        hp, hc = ts.copy(), ts.copy()
        hp.data = np.real(hp.data)
        hc.data = np.imag(hc.data)
        fd_phase = np.unwrap(np.angle(fs))
        fd_phase = fd_phase - fd_phase[0]
        td_phase = phase_from_polarizations(hp, hc).data
        return fd_phase, td_phase

    @staticmethod
    def polarizations_from_frame(ts, fs):
        hp = np.real(ts.data)
        hc = np.imag(ts.data)
        sp = np.real(fs.data)
        sc = np.imag(fs.data)
        return sp, sc, hp, hc


class GenerateSignal(object):
    def __init__(self, fs, type, ifo):
        self.ifo = ifo
        if type == "Mod" or type == "ModGR":
            fs._data *= self.get_corr_factor(fs.sample_frequencies)
            self.file = "ModGR Generated"
        else:
            self.file = "PureGR Generated"
        self.freqs = fs.sample_frequencies
        self.amp_fd = np.abs(fs.data)
        self.phase_fd = np.unwrap(np.angle(fs))

    def get_corr_factor(self, freqs):
        fz = DEFAULT_FARR_PARAMS["fz"]
        deltaf = DEFAULT_FARR_PARAMS["deltaf"]
        refreq = DEFAULT_FARR_PARAMS["refreq"]
        dphase = DEFAULT_FARR_PARAMS["dphase"]
        dfz = DEFAULT_FARR_PARAMS["dfz"]
        damp = DEFAULT_FARR_PARAMS["damp"]
        deltapsi = 0.5 * np.tanh((freqs - fz) / deltaf)
        deltapsi = deltapsi - 0.5 * np.tanh((refreq - fz) / deltaf)
        deltapsi = dphase * deltapsi
        dlogamp = 0.5 * (np.tanh((freqs - fz - dfz) / deltaf)) * (np.tanh((freqs - fz - dfz) / deltaf))
        dlogamp = dlogamp - 0.5 * np.tanh((refreq - fz) / deltaf) * np.tanh((refreq - fz) / deltaf)
        dlogamp = damp * dlogamp
        amp = np.exp(dlogamp)
        return amp * ((2.0 + 1j * deltapsi) / (2.0 - 1j * deltapsi))


class SignalDict(object):
    fixed_waveform_params = {
        "delta_f": 1.0 / 32.0,
        "seglen": 32.0,
        "srate": 4096.0,
        "f_lower": 10.0,
        "f_final": 4096.0 / 2.0,
        "delta_t": 1.0 / 4096.0,
    }
    epoch = 1126259462

    def __init__(self, paths, ifos, signal_args, approx="IMRPhenomD"):
        self.dict = {}
        self.generator = None
        self.files = paths
        if len(paths) == len(ifos):
            for path, ifo, args in zip(paths, ifos, signal_args):
                self.dict[ifo] = Signal(path, ifo, **args)
        else:
            self.generator = self.setup_generator(ifos, approx)
            dtilde = self.generator.generate(**DEFAULT_INJECTED_PARAMS)
            for i in ifos:
                self.dict[i] = GenerateSignal(dtilde[i], paths[0], i)

    def __call__(self, ifo, *args, **kwargs):
        return self.dict[ifo]

    def setup_generator(self, ifos, approx):

        generator = FDomainDetFrameGenerator(
            FDomainCBCGenerator,
            self.epoch,
            variable_args=PYCBC_WAVEFORM_PARAMS,
            detectors=ifos,
            **self.fixed_waveform_params,
            approximant=approx,
        )
        return generator


class DataDump(object):
    def __init__(self, post_files, labels, ifos, flow=20.0, fhigh=2048.0, seglen=8, trig=4, logger=None):
        self.posts = post_files
        self.labels = labels
        self.ifos = ifos
        self.frange = (flow, fhigh)
        self.logger = logger
        self.trange = (trig - seglen + 2, trig + 2)

    def get_data(self):
        data = {}
        for file, label in zip(self.posts, self.labels):
            data[label] = {}
            for ifo in self.ifos:
                data[label][ifo] = {}
                for suffix in DATADUMP_SUFFIXS:
                    data[label][ifo][suffix] = {}
                    ddfile = f"{file}{ifo}{suffix}"
                    if os.path.isfile(ddfile):
                        self.logger.debug(f"Using DataDump file {ddfile}")
                    else:
                        self.logger.debug(f"Data dump file {ddfile} NOT FOUND")
                        continue
                    if "freq" in suffix:
                        (
                            data[label][ifo][suffix]["freqs"],
                            data[label][ifo][suffix]["hp"],
                            data[label][ifo][suffix]["hc"],
                        ) = np.genfromtxt(ddfile, unpack=True)
                    else:
                        data[label][ifo][suffix]["times"], data[label][ifo][suffix]["data"] = np.genfromtxt(
                            ddfile, unpack=True
                        )
        return data

    def make_plots(self, outdir):
        outdir = outdir + "/datadump/"
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        data = self.get_data()
        for file, label in zip(self.posts, self.labels):
            for ifo in self.ifos:
                for suffix in DATADUMP_SUFFIXS:
                    ddfile = f"{file}{ifo}{suffix}"
                    if os.path.isfile(ddfile):
                        plotName = outdir + f"{label}_{ifo}_{suffix[:-4]}.png"
                        if "time" in suffix:
                            plot = self.plot_td(plotName, data, ifo, label, suffix)
                            plt.close(plot)
                        else:
                            plot = self.plot_fd(plotName, data, ifo, label, suffix)
                            plt.close(plot)
                    else:
                        self.logger.debug(f"FILE {ddfile} not found, not plotting {suffix[:-4]}")

    def plot_td(self, filename, data, ifo, label, suffix):
        t = data[label][ifo][suffix]["times"]
        ts = data[label][ifo][suffix]["data"]
        fig = plt.figure(figsize=(10, 8))
        plt.plot(t, ts, label=ifo)
        plt.xlabel("Time (s)")
        plt.ylabel("Strain (m)")
        plt.savefig(filename, dpi=300)
        plt.xlim(self.trange)
        # plt.show()
        return fig

    def plot_fd(self, filename, data, ifo, label, suffix):
        f = data[label][ifo][suffix]["freqs"]
        hp = data[label][ifo][suffix]["hp"]
        hc = data[label][ifo][suffix]["hc"]
        fig = plt.figure(figsize=(10, 8))
        fs = hp + 1j * hc
        plt.loglog(f, np.abs(fs), label=ifo)
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Waveform Amplitude")
        plt.xlim(self.frange)
        plt.savefig(filename, dpi=300)
        # plt.show()
        return fig


def load_frequencyseries(path, group=None):
    """
    Load a FrequencySeries from a .hdf, .txt or .npy file. The
    default data types will be double precision floating point.

    Parameters
    ----------
    path: string
        source file path. Must end with either .npy or .txt.

    group: string
        Additional name for internal storage use. Ex. hdf storage uses
        this as the key value.

    Raises
    ------
    ValueError
        If path does not end in .npy or .txt.
    """
    ext = os.path.splitext(path)[1]
    if ext == ".npy":
        data = np.load(path)
    elif ext == ".txt":
        data = np.loadtxt(path)
    elif ext == ".hdf":
        key = "data" if group is None else group
        f = h5py.File(path, "r")
        data = f[key][:]
        series = FrequencySeries(data, delta_f=f[key].attrs["delta_f"], epoch=f[key].attrs["epoch"])
        f.close()
        return series
    else:
        raise ValueError("Path must end with .npy, .hdf, or .txt")

    if data.ndim == 2:
        delta_f = (data[-1][0] - data[0][0]) / (len(data) - 1)
        return FrequencySeries(data[:, 1], delta_f=delta_f)
    elif data.ndim == 3:
        delta_f = (data[-1][0] - data[0][0]) / (len(data) - 1)
        return FrequencySeries(data[:, 1] + 1j * data[:, 2], delta_f=delta_f)
    else:
        raise ValueError(
            "File has %s dimensions, cannot convert to Array, \
                          must be 2 (real) or 3 (complex)"
            % data.ndim
        )


if __name__ == "__main__":
    data_dump = DataDump(
        [
            "/home/bedelman/PycharmProjects/sigsplinepostproc/scratch/datadump/lalinference_mcmc_sept24_noisetest_nosplineFD.hdf5"
        ],
        ["nospline"],
        ["H1"],
    )
    data_dump.make_plots(".")
