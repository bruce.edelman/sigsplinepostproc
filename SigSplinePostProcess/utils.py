import numpy as np
import logging, os
import matplotlib.pyplot as plt
import h5py, json
from scipy.interpolate import CubicSpline

__name__ = "Spsig_PostProc"
logger = logging.getLogger(__name__)

POSSIBLE_WAVEFORM_PARAMS = [
    "mass1",
    "mass2",
    "spin1x",
    "spin2x",
    "spin1y",
    "spin2y",
    "spin1z",
    "spin2x",
    "eccentricity",
    "lambda1",
    "lambda2",
    "distance",
    "coa_phase",
    "inclination",
    "q",
    "chirpmass",
    "logdistance",
    "a_spin1",
    "a_spin2",
    "declination",
    "rightascension",
    "polarisation",
    "time",
]

BILBY_JSON_PARAMS = [
    "mass_1",
    "mass_2",
    "a_1",
    "a_2",
    "tilt_1",
    " tilt_2",
    "phi_12",
    "phi_jl",
    "luminosity_distance",
    "theta_jn",
    "psi",
    "phase",
    "geocent_time",
    "ra",
    "dec",
]

JSON_TO_PYCBC_PARAMS = {"luminosity_distance": "distance", "phase": "coa_phase"}

LAL_APPROX_LIST = [
    "TaylorT1",
    "TaylorT2",
    "TaylorT3",
    "TaylorF1",
    "EccentricFD",
    "TaylorF2",
    "TaylorF2Ecc",
    "TaylorF2NLTides",
    "TaylorR2F4",
    "TaylorF2RedSpin",
    "TaylorF2RedSpinTidal",
    "PadeT1",
    "PadeF1",
    "EOB",
    "BCV",
    "BCVSpin",
    "SpinTaylorT1",
    "SpinTaylorT2",
    "SpinTaylorT3",
    "SpinTaylorT4",
    "SpinTaylorT5",
    "SpinTaylorF2",
    "SpinTaylorFrameless",
    "SpinTaylor",
    "PhenSpinTaylor",
    "PhenSpinTaylorRD",
    "SpinQuadTaylor",
    "FindChirpSP",
    "FindChirpPTF",
    "GeneratePPN",
    "BCVC",
    "FrameFile",
    "AmpCorPPN",
    "NumRel",
    "NumRelNinja2",
    "Eccentricity",
    "EOBNR",
    "EOBNRv2",
    "EOBNRv2HM",
    "EOBNRv2_ROM",
    "EOBNRv2HM_ROM",
    "TEOBResum_ROM",
    "SEOBNRv1",
    "SEOBNRv2",
    "SEOBNRv2_opt",
    "SEOBNRv3",
    "SEOBNRv3_pert",
    "SEOBNRv3_opt",
    "SEOBNRv3_opt_rk4",
    "SEOBNRv4",
    "SEOBNRv4_opt",
    "SEOBNRv2T",
    "SEOBNRv4T",
    "SEOBNRv1_ROM_EffectiveSpin",
    "SEOBNRv1_ROM_DoubleSpin",
    "SEOBNRv2_ROM_EffectiveSpin",
    "SEOBNRv2_ROM_DoubleSpin",
    "SEOBNRv2_ROM_DoubleSpin_HI",
    "Lackey_Tidal_2013_SEOBNRv2_ROM",
    "SEOBNRv4_ROM",
    "SEOBNRv4_ROM_NRTidal",
    "SEOBNRv4T_surrogate",
    "HGimri",
    "IMRPhenomA",
    "IMRPhenomB",
    "IMRPhenomFA",
    "IMRPhenomFB",
    "IMRPhenomC",
    "IMRPhenomD",
    "IMRPhenomD_NRTidal",
    "IMRPhenomHM",
    "IMRPhenomP",
    "IMRPhenomPv2",
    "IMRPhenomPv2_NRTidal",
    "IMRPhenomFC",
    "TaylorEt",
    "TaylorT4",
    "EccentricTD",
    "TaylorN",
    "SpinTaylorT4Fourier",
    "SpinTaylorT5Fourier",
    "SpinDominatedWf",
    "NR_hdf5",
    "NRSur4d2s",
    "NRSur7dq2",
    "SEOBNRv4HM",
    "NRHybSur3dq8",
    "NumApproximants",
]


class EmptyLogger(object):
    def info(self, str):
        print(str)

    def debug(self, str):
        print(str)


def lal_approx_number_to_string(approx):
    return LAL_APPROX_LIST[int(approx)]


def chunk_list(l, n):
    return [l[i : i + n] for i in range(0, len(l), n)]


def get_burn(l):
    ct = 0
    for i in l:
        if i > 0:
            break
        else:
            ct += 1
    return ct


def get_near(logf, amps, fs):
    logf = np.exp(logf)
    dist = np.inf
    ind = 0
    for i, f in enumerate(fs):
        tmp = np.abs(f - logf)
        if tmp < dist:
            dist = tmp
            ind = i
    return amps[ind]


def str2bool(s):
    if s == "True":
        return True
    elif s == "False":
        return False
    elif s is False:
        return s
    elif s is True:
        return s
    else:
        raise ValueError("Input to Str2Bool is not a string of False or True or Bool")


def creat_postproc_script(files, labels, outdir, manburns=None, run=False):
    if os.path.isfile(outdir + "postproc.sh"):
        os.remove(outdir + "postproc.sh")

    with open(outdir + "postproc.sh", "w+") as file:
        file.write("#!/bin/sh\n")
        for data, label in zip(files, labels):
            out = f"{outdir}posplots_{label}/" if run else f".{outdir}posplots_{label}/"
            if manburns is not None:
                command = f"cbcBayesPostProc -o {out} -d {data} --fixedBurnin {manburns[label]}\n"
            else:
                command = f"cbcBayesPostProc -o {out} -d {data}\n"
            file.write(command)
    make_executable(outdir + "postproc.sh")
    return outdir + "postproc.sh"


def setup_logger(outdir=None, label=None, log_level="INFO"):

    if type(log_level) is str:
        try:
            level = getattr(logging, log_level.upper())
        except AttributeError:
            raise ValueError("log_level {} not understood".format(log_level))
    else:
        level = int(log_level)

    logger = logging.getLogger(__name__)
    logger.propagate = False
    logger.setLevel(level)

    if any([type(h) == logging.StreamHandler for h in logger.handlers]) is False:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(
            logging.Formatter("%(asctime)s %(name)s %(levelname)-8s: %(message)s", datefmt="%H:%M")
        )
        stream_handler.setLevel(level)
        logger.addHandler(stream_handler)

    if any([type(h) == logging.FileHandler for h in logger.handlers]) is False:
        if label:
            if outdir:
                if not os.path.exists(outdir):
                    os.mkdir(outdir)
            else:
                outdir = "./logs/"
            log_file = "{}/{}.log".format(outdir, label)
            file_handler = logging.FileHandler(log_file)
            file_handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s: %(message)s", datefmt="%H:%M"))

            file_handler.setLevel(level)
            logger.addHandler(file_handler)

    for handler in logger.handlers:
        handler.setLevel(level)

    return logger


def get_post_from_json(pos):
    params = pos["posterior"]["content"].keys()
    arr = np.zeros((len(pos["posterior"]["content"]["mass_1"]),), dtype=[(name, "f8") for name in params])
    for name in params:
        arr[name] = np.array(pos["posterior"]["content"][name])
    return arr


def get_waveform_params_from_h5(posterior):
    params_present = []
    params = posterior.dtype.names
    for param in params:
        if param in POSSIBLE_WAVEFORM_PARAMS:
            params_present.append(param)
    return params_present


def convert_JSON_wv_params_to_PYCBC(posterior):
    for p in JSON_TO_PYCBC_PARAMS.keys():
        posterior[JSON_TO_PYCBC_PARAMS[p]] = posterior[p]
        posterior.pop(JSON_TO_PYCBC_PARAMS[p])
    return posterior


def get_waveform_params_from_json(posterior):
    params_present = []
    params = posterior.keys()
    for param in params:
        if param in POSSIBLE_WAVEFORM_PARAMS:
            params_present.append(param)
    return params_present


def check_yes_no_input(inp):
    letters = [char for char in inp]
    return True if letters[0] == "y" else False


def make_executable(path):
    """
    found on https://stackoverflow.com/questions/12791997/how-do-you-do-a-simple-chmod-x-from-within-python
    :param path:
    :return:
    """
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2  # copy R bits to X
    os.chmod(path, mode)


def create_runscript(args, outdir, rerun=False):
    n_dirs = len(outdir.split("/")) - 2
    added = "."
    if n_dirs == 2:
        added = "../."
    command_line = " ".join(args)
    outlist = list(command_line)
    chars_added = 0
    if not rerun:
        for i, char in enumerate(list(command_line)):
            if i + 1 == len(list(command_line)):
                break
            elif char == "." and list(command_line)[i + 1] == "/":
                outlist.insert(i + chars_added, added)
                chars_added += len(added)

    command_line = "".join(outlist)
    if not rerun:
        command_line += " --rerun"
    cli_file = open(outdir + "rerun.sh", "w+")
    cli_file.write(command_line)
    cli_file.close()
    make_executable(outdir + "rerun.sh")
    return outdir + "rerun.sh"


def plot(ns, pos, key, save=True, outdir=None, show=False):
    lowval = np.percentile(pos[key], 10)
    highval = np.percentile(pos[key], 90)
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10, 6))
    if ns is not None:
        low = np.percentile(ns[key], 10)
        high = np.percentile(ns[key], 90)
        axs[0].plot(ns[key], label="spsig off", color="orange")
        axs[0].axhline(low, ls="-", color="orange")
        axs[0].axhline(high, ls="-", color="orange")
        # print(ns[key].shape)
        # print(ns[key])
        try:
            axs[1].hist(ns[key], density=True, alpha=0.35, label="spsig off", color="orange")
        except IndexError:
            plt.close("all")
            return
        axs[1].axvline(low, ls="-", color="orange")
        axs[1].axvline(high, ls="-", color="orange")
    axs[0].plot(pos[key], label="spsig on", color="b")
    axs[0].axhline(lowval, ls="-", color="b")
    axs[0].axhline(highval, ls="-", color="b")
    axs[0].legend()
    axs[0].set_xlabel("Iteration")
    axs[0].set_ylabel(key)
    axs[1].hist(pos[key], density=True, alpha=0.35, bins=40, label="spsig on", color="b")
    axs[1].axvline(lowval, ls="-", color="b")
    axs[1].axvline(highval, ls="-", color="b")
    axs[1].set_xlabel(key)
    axs[1].legend()
    filename = outdir + "{}_post.png".format(key) if outdir is not None else "./{}_post.png".format(key)
    if save:
        plt.savefig(filename)
    if show:
        plt.show()
        plt.close("all")


def check_posterior_params(ns, pos, logger=None, **kwargs):
    nospline_params = ns.dtype.names
    pos_params = pos.dtype.names
    for param in pos_params:
        if param in nospline_params:
            lowval = np.percentile(pos[param], 10)
            highval = np.percentile(pos[param], 90)
            if (np.mean(ns[param]) < highval) and (np.mean(ns[param]) > lowval):
                if logger is None:
                    print(
                        "{} mean value in nospline run lies in 80% CI of spline run: {} lies in range = [{}, {}]"
                        " plotting posteriors...".format(param, np.mean(ns[param]), lowval, highval)
                    )
                else:
                    logger.info(
                        "{} mean value in nospline run lies in 80% CI of spline run: {} lies in range = "
                        "[{}, {}] plotting posteriors...".format(param, np.mean(ns[param]), lowval, highval)
                    )
            else:
                if logger is None:
                    print(
                        "{} mean value in nospline does not lie in 80% CI range: plotting posteriors...".format(param)
                    )
                else:
                    logger.info(
                        "{} mean value in nospline does not lie in 80% CI range: plotting posteriors...".format(param)
                    )
            plot(ns, pos, param, **kwargs)
        else:
            plot(None, pos, param, **kwargs)
        plt.close("all")


def get_posts_from_files(files, manburn=[0, 0], burn=False):
    if len(files) != 2:
        raise ValueError("Must pass a nospline file and a spline run file both and only: ")
    if files[0].split(".")[-1] == "json":
        _dat_file = open(files[0], "r")
        posterior = json.load(_dat_file)
        posterior = get_post_from_json(posterior)
    else:
        posterior = h5py.File(files[0], "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
        if burn:
            burnin = get_burn(posterior["cycle"])
            if burnin < 0.95 * len(posterior):
                posterior = posterior[int(burnin) :]
    pos = posterior[int(manburn[0]) :]
    if files[1].split(".")[-1] == "json":
        _dat_file = open(files[1], "r")
        nosp = json.load(_dat_file)
        nosp = get_post_from_json(nosp)
    else:
        nosp = h5py.File(files[1], "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
        if burn:
            burnin = get_burn(nosp["cycle"])
            if burnin < 0.95 * len(nosp):
                nosp = nosp[int(burnin) :]
    ns = nosp[int(manburn[1]) :]
    return pos, ns


def generate_spline_factors(infile, attrs=None, burn=True):
    posterior = h5py.File(infile, "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
    if attrs is None:
        attrs = posterior.attrs
    flow = attrs["flow"]
    srate = attrs["sampleRate"]
    seglen = attrs["segmentLength"]
    nnodes = attrs["spsig_npts"]
    N = int(float(srate) * float(seglen) / 2 + 1)
    dF = 1.0 / float(seglen)
    freqs = np.arange(N) * dF
    fhigh = float(srate) / 2

    if burn:
        burnin = get_burn(posterior["cycle"])
        posterior = posterior[int(burnin) :]
        if len(posterior) < 10:
            raise ValueError("Burnin removed too many samples try again with burn=False kwarg")

    pos_len = len(posterior)

    phis = np.ones((pos_len, len(freqs)), dtype=np.complex)
    amps = np.zeros_like(phis, dtype=np.float)
    sigF = np.empty_like(phis, dtype=np.complex)
    logfs = np.zeros((pos_len, nnodes), dtype=np.float)

    for k, samp in enumerate(posterior):
        amp_nodes = np.zeros(nnodes)
        lfreqs = np.zeros(nnodes)
        dA = np.empty(len(freqs))
        dPhi = np.empty_like(dA)
        for i in range(nnodes):
            try:
                amp_nodes[i] = samp["spsig_amp_{}".format(i)]
            except ValueError:
                amp_nodes[i] = attrs["spsig_amp_{}".format(i)]
            try:
                lfreqs[i] = samp["spsig_logfreq_{}".format(i)]
            except ValueError:
                lfreqs[i] = attrs["spsig_logfreq_{}".format(i)]

        dphi = CubicSpline(lfreqs, [samp["spsig_phase_{}".format(f)] for f in range(nnodes)])
        da = CubicSpline(lfreqs, amp_nodes)
        sel = (freqs > flow) & (freqs < fhigh)
        dA[sel] = da(np.log(freqs[sel]))
        dPhi[sel] = dphi(np.log(freqs[sel]))
        amps[k, :][sel] = dA[sel]
        phis[k, :][sel] = (2.0 + 1j * dPhi[sel]) / (2.0 - 1j * dPhi[sel])
        sigF[k, :] = (1.0 + amps[k, :]) * phis[k, :]
        logfs[k, :] = lfreqs
    return {"amps": amps, "phis": phis, "sigFactor": sigF, "logfs": logfs, "freqs": freqs}


def get_ifos_from_post(posterior):
    ifos = {"H1": False, "L1": False, "V1": False}
    for name in posterior.dtype.names:
        pref = name[:2]
        if pref in ifos.keys() and not ifos[pref]:
            ifos[pref] = True
    return ifos
