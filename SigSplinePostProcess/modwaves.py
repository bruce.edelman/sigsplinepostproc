import numpy as np
import h5py, csv, os
from .pbar import progress_bar
from .utils import get_burn, get_waveform_params_from_h5, lal_approx_number_to_string, EmptyLogger
from .splines import generate_spline_factors
from pycbc.waveform.utils import phase_from_polarizations, amplitude_from_polarizations, amplitude_from_frequencyseries
from pycbc.waveform import fd_approximants, td_approximants
from pycbc.waveform.generator import FDomainCBCGenerator, FDomainDetFrameGenerator, TDomainCBCGenerator
from pycbc.conversions import mass1_from_mchirp_q, mass2_from_mchirp_q
from ligo.gracedb.rest import GraceDb

OPT_WAVE_NUM = 300
PYCBC_WAVEFORM_PARAMS = ["mass1", "mass2", "distance", "spin1z", "spin2z", "tc", "ra", "dec", "polarization"]


def get_t0_from_gracedb(eventid, logger):
    logger.info("grabbing event {} from GraceDb api...".format(eventid))
    gdb_client = GraceDb()
    if eventid[0] == "S":
        resp = gdb_client.superevent(eventid)
    else:
        resp = gdb_client.event(eventid)
    return resp.json()["t_0"]


class ModWaves(object):
    def __init__(
        self,
        input_file,
        outdir,
        logger=EmptyLogger(),
        ifos=None,
        man_burn=0,
        thin="auto",
        time_domain=False,
        label=None,
        no_reconstruct=False,
        event=None,
        signal=None,
        **kwargs,
    ):

        self.label = label
        self.ptmcmc_file = input_file
        self._data_file = h5py.File(input_file, "r")
        self.posterior = self._data_file["lalinference"]["lalinference_mcmc"]["posterior_samples"]
        self.attributes = self.posterior.attrs
        self.srate = self.attributes["sampleRate"]
        self.seglen = self.attributes["segmentLength"]
        self.signal = signal
        burn = int(get_burn(self.posterior["cycle"]))
        man_burn = int(man_burn)
        if man_burn + burn > 0.95 * len(self.posterior):
            logger.info("Manual Burnin + regular burnin is greater than posterior allows:")
            if burn > 0.95 * len(self.posterior):
                self.removed = 0
                logger.info("cycle burnin is greater than posterior allows:")
                pass
            else:
                self.removed = burn
                logger.info(f"removing {burn}/{len(self.posterior)} samples from posterior")
                self.posterior = self.posterior[burn:]
        else:
            self.removed = burn + man_burn
            logger.info(f"removing {burn+man_burn}/{len(self.posterior)} samples from posterior")
            self.posterior = self.posterior[man_burn + burn :]
        self.pos_len = len(self.posterior)
        self.params = self.posterior.dtype.names
        self.no_reconstruct = no_reconstruct
        self.ifos = ifos
        self.logger = logger
        self.TD = time_domain
        self.pbar = True
        self.flow = self.attributes["flow"]
        self.fhigh = self.attributes["sampleRate"] / 2.0
        if event is not None:
            if event[0:2] == "GW":
                from pycbc.catalog import Merger

                if "_" in event:
                    event_name = event.split("_")[0]
                else:
                    event_name = event
                m = Merger(event_name)
                self.epoch = m.tc
                self.added = 0
            elif event[0] == "S" or event[0] == "G":
                self.epoch = get_t0_from_gracedb(event, logger)
                self.added = 0
            else:
                raise ValueError("event {} name not understood to get merger time from pycbc or gracedb".format(event))
        else:
            self.epoch = 1126259462
            self.added = self.epoch - self.seglen
        if thin == "auto":
            if self.pos_len <= OPT_WAVE_NUM:
                self.thin = 1
            else:
                self.thin = int(self.pos_len) // int(OPT_WAVE_NUM)
        elif not isinstance(thin, str):
            self.thin = thin
        else:
            self.thin = 1
        self.dump_dict = {name: [] for name in PYCBC_WAVEFORM_PARAMS}
        self.approx = lal_approx_number_to_string(self.attributes["LAL_APPROXIMANT"])
        self.logger.info("ModWaves Object setup with Approx = {}: ".format(self.approx))
        if len(ifos) > self.attributes["nIFO"]:
            raise ValueError("passed in ifos more than used in PE run at file: {}".format(input_file))
        if "spsig_logfreq_0" in self.attributes:
            self.spline = True
            self.spline_data = generate_spline_factors(self.posterior, self.attributes)
            self.sigF = self.spline_data["sigFactor"]
            self.spline_len = self.sigF.shape[0]
        else:
            self.spline = False
            self.spline_data = None
        waveform_params = get_waveform_params_from_h5(self.posterior)
        self.pipeline_args = kwargs
        self.wvdict = {}
        self.outdir = outdir
        self.modwaves_outfile = outdir + "mod_waves_{}.h5".format(input_file.split("/")[-1])
        self.wv_dump_file = outdir + "mod_waves_{}_{}_params.csv".format(input_file.split("/")[-1], self.approx)
        for p in waveform_params:
            self.wvdict[p] = self.posterior[p]
        self.wvdict["mass1"], self.wvdict["mass2"] = (
            mass1_from_mchirp_q(self.wvdict["chirpmass"], 1.0 / self.wvdict["q"]),
            mass2_from_mchirp_q(self.wvdict["chirpmass"], 1.0 / self.wvdict["q"]),
        )
        self.fixed_wv_params = {
            "delta_f": 1.0 / self.attributes["segmentLength"],
            "approximant": self.approx,
            "f_lower": self.attributes["flow"],
            "f_final": self.attributes["sampleRate"] / 2.0,
            "delta_t": 1.0 / self.attributes["sampleRate"],
        }
        if not os.path.isfile(self.modwaves_outfile):
            self.generator = self.frame_gen()
            self.write_mw_out_file(self.modwaves_outfile)
        else:
            self.logger.info("Loading old modwaves file in directory at {}".format(self.modwaves_outfile))
        self._data_file.close()

    def frame_gen(self):
        if self.approx in fd_approximants():
            self.logger.info("Approx = {} found in pycbc fd_approximants".format(self.approx))
            gen = FDomainCBCGenerator
        elif self.approx in td_approximants():
            self.logger.info("Approx = {} found in pycbc td_approximants".format(self.approx))
            gen = TDomainCBCGenerator
        else:
            self.logger.info("Approx not found in pycbc fd_approximants or td_approximants using IMRPhenomD")
            self.approx = "IMRPhenomD"
            gen = FDomainCBCGenerator
            self.fixed_wv_params["approximant"] = self.approx
        generator = FDomainDetFrameGenerator(
            gen, self.epoch, variable_args=PYCBC_WAVEFORM_PARAMS, detectors=self.ifos, **self.fixed_wv_params
        )
        return generator

    def generate_waveform(self, idx):
        m1 = self.wvdict["mass1"][idx]
        m2 = self.wvdict["mass2"][idx]
        d = np.exp(self.wvdict["logdistance"][idx])
        spin1z = self.wvdict["a_spin1"][idx]
        spin2z = self.wvdict["a_spin2"][idx]
        ra = self.wvdict["rightascension"][idx]
        dec = self.wvdict["declination"][idx]
        t = self.wvdict["time"][idx] + self.added
        pol = self.wvdict["polarisation"][idx]
        vals = [m1, m2, d, spin1z, spin2z, t, ra, dec, pol]
        for key, val in zip(PYCBC_WAVEFORM_PARAMS, vals):
            self.dump_dict[key].append(val)
        htilde = self.generator.generate(
            mass1=m1,
            mass2=m2,
            distance=d,
            spin1z=spin1z,
            spin2z=spin2z,
            tc=t,
            polarization=pol,
            ra=ra,
            dec=dec,
            **self.fixed_wv_params,
        )
        return htilde

    def write_mw_out_file(self, file):
        of = h5py.File(file, "w")
        of.attrs["PTMCMC_File"] = self.ptmcmc_file
        of.attrs["approx"] = self.approx
        of.attrs["pos_len"] = self.pos_len
        if self.spline:
            of.attrs["nodes"] = self.attributes["spsig_npts"]

        ifo_groups = []
        self.logger.info("Writing spline data to File ...")
        for ifo in self.ifos:
            det_grp = of.create_group(ifo)
            ifo_groups.append(det_grp)
            if self.TD:
                time_dom_grp = det_grp.create_group("td")
                time_dom_grp.attrs["dt"] = 1.0 / self.attributes["sampleRate"]
            freq_dom_grp = det_grp.create_group("fd")
            freq_dom_grp.attrs["df"] = 1.0 / self.attributes["segmentLength"]
            freq_dom_grp.attrs["flow"] = self.attributes["flow"]
            freq_dom_grp.attrs["fhigh"] = self.attributes["sampleRate"] / 2.0
            if self.spline:
                flen = self.sigF.shape[1]
                freq_dom_grp.create_dataset("sigF", (self.spline_len, flen), data=self.sigF, chunks=True)
                freq_dom_grp.create_dataset("amps", (self.spline_len, flen), data=self.spline_data["amps"], chunks=True)
                freq_dom_grp.create_dataset("phis", (self.spline_len, flen), data=self.spline_data["phis"], chunks=True)
                freq_dom_grp.create_dataset(
                    "logfs",
                    (self.spline_len, self.attributes["spsig_npts"]),
                    data=self.spline_data["logfs"],
                    chunks=True,
                )
            if self.signal is None:
                maxl_idx = self.get_maxl_idx()
                maxl_fs = self.generate_waveform(maxl_idx)
                if self.spline:
                    maxl_fs[ifo]._data *= self.sigF[maxl_idx, :]
                ampf = amplitude_from_frequencyseries(maxl_fs[ifo])
                phase_series_f = np.unwrap(np.angle(maxl_fs[ifo]))
                maxl_grp = freq_dom_grp.create_group("maxl_wave")
                maxl_grp.create_dataset("amp", data=ampf)
                maxl_grp.create_dataset("phase", data=phase_series_f)

        if not self.no_reconstruct:
            ct = 0
            for wave in self.populate_waves():
                self.save_wave_data(wave, ct, of)
                ct += 1
            self.dump_params(self.wv_dump_file)
        of.close()
        return file

    def get_maxl_idx(self):
        with h5py.File(self.ptmcmc_file, "r") as inp:
            post = inp["lalinference"]["lalinference_mcmc"]["posterior_samples"]
            post = post[self.removed :]
            return np.argmax(post["logl"])

    def dump_params(self, f):
        with open(f, "w") as outfile:
            writer = csv.writer(outfile)
            writer.writerow(self.dump_dict.keys())
            writer.writerows(zip(*self.dump_dict.values()))

    def save_wave_data(self, wave, ct, of):
        total = len(range(0, self.pos_len, self.thin))
        for det in self.ifos:
            RootGroup = of[det]
            wave_data = wave[det]
            fd = RootGroup["fd"]
            fd_waves = wave_data["fd"]
            datasets = [fd]
            if self.TD:
                td = RootGroup["td"]
                td_waves = wave_data["td"]
                datasets.append(td)

            if ct == 0:
                freqs = fd_waves["freqs"]
                self.flen = len(freqs)
                fd.create_dataset("freqs", data=freqs)
                if self.TD:
                    times = td_waves["times"]
                    self.tlen = len(times)
                    td.create_dataset("times", data=times)

                for dom in datasets:
                    mod_waves = dom.create_group("mod_waves")
                    for data in ["amp", "phase"]:
                        if dom != fd:
                            mod_waves.create_dataset(
                                data, (1, self.tlen), data=td_waves[data], chunks=True, maxshape=(total + 1, self.tlen)
                            )
                        else:
                            mod_waves.create_dataset(
                                data, (1, self.flen), data=fd_waves[data], chunks=True, maxshape=(total + 1, self.flen)
                            )
            else:
                for dom in datasets:
                    mod_waves = dom["mod_waves"]
                    for data in ["amp", "phase"]:
                        mod_waves[data].resize(ct + 1, axis=0)
                        if dom != fd:
                            mod_waves[data][ct, :] = td_waves[data]
                        else:
                            mod_waves[data][ct, :] = fd_waves[data]

    def populate_waves(self):
        times, freqs = None, None
        total = len(range(0, self.pos_len, self.thin))
        hyper_outDict = {det: None for det in self.ifos}
        with progress_bar(self.pbar, total) as prg_bar:
            for i in range(0, self.pos_len, self.thin):
                stilde = self.generate_waveform(i)
                for ifo in self.ifos:
                    if self.spline:
                        stilde[ifo]._data *= self.sigF[i, :]
                    ampf = amplitude_from_frequencyseries(stilde[ifo])
                    phase_series_f = np.unwrap(np.angle(stilde[ifo]))

                    if self.TD:
                        htilde = stilde.copy()
                        hts = htilde[ifo].to_timeseries(delta_t=1 / self.srate)
                        pl = hts.real()
                        cr = hts.imag()
                        ampt = amplitude_from_polarizations(pl, cr)
                        phase_series_t = phase_from_polarizations(pl, cr)

                    if times is None and freqs is None:
                        if self.TD:
                            times = pl.sample_times
                        freqs = stilde[ifo].sample_frequencies
                    if not self.TD:
                        out_dict = {"td": None, "fd": {"amp": ampf.data, "phase": phase_series_f, "freqs": freqs}}
                    else:
                        out_dict = {
                            "td": {"amp": ampt.data, "phase": phase_series_t.data, "times": times},
                            "fd": {"amp": ampf.data, "phase": phase_series_f, "freqs": freqs},
                        }
                    hyper_outDict[ifo] = out_dict
                prg_bar.update(1)
                yield hyper_outDict
