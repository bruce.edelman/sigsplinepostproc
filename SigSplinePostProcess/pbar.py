"""
This File uses the same method as github.com/dfm/emcee/emcee/pbar.py to set up the tqdm progress bar
"""
try:
    import tqdm
except ImportError:
    tqdm = None


# Set up a dummy class if we don't want a progress bar
class _NoProgress(object):
    """
    dummy wrapper class if we don't use a progress bar
    """

    def __init__(self):
        pass

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, *args, **kwargs):
        pass

    def update(self, ct):
        pass


"""
This function will call the tqdm progress bar for the mcmc chain same as used in the reference pytthon file listed above
"""


def progress_bar(progress, total):
    """
    This function calls the tqdm progress bar or our dummy no progress bar class if we dont want to use one
    :param progress: this is a bool or string, if bool False will return the dummy bar, and true the default tqdm bar,
    if string must be a string to call a specific tqdm progress bar
    :param total: int that is the size of the progerss bar
    :return: this returns a tqdm progress bar object or our dummy wrapper class
    """

    if progress:
        if tqdm is None:
            return _NoProgress()
        else:
            if progress is True:
                return tqdm.tqdm(total=total)
            else:
                return getattr(tqdm, "tqdm_" + progress)(total=total)

    else:
        return _NoProgress
