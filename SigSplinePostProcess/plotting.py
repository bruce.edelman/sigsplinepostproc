import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple
from .utils import get_burn, get_post_from_json, generate_spline_factors, get_near, EmptyLogger
import h5py, json, corner
from .pbar import progress_bar
from celluloid import Camera
from matplotlib.gridspec import GridSpec
import matplotlib.lines as mlines
from random import shuffle
from pycbc.conversions import mass1_from_mchirp_q, mass2_from_mchirp_q


COLOR_CYCLE = [
    "#332288",
    "#CC6677",
    "#88CCEE",
    "#DDCC77",
    "#44AA99",
    "#117733",
    "#882255",
    "#999933",
    "#AA4499",
    "#65374D",
    "#C99D99",
    "#DD7703",
    "#44A679",
]
COLOR_CYCLE = COLOR_CYCLE[::-1]
# shuffle(COLOR_CYCLE)
PYCBC_WAVEFORM_PARAMS = ["mass1", "mass2", "distance", "spin1z", "spin2z", "tc", "ra", "dec", "polarization"]


def plot_modified_waveform_amp_fd(
    file,
    lvls=[50, 90, 95],
    filename=None,
    out_pref="",
    dpi=300,
    signal=None,
    show=False,
    no_mod_waves=None,
    pos_samps=False,
    psd=None,
    grid=True,
    ifos=["H1"],
    flow=None,
    fhigh=None,
    maxl=False,
):
    mod_waves = h5py.File(file, "r")
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    pref = file.split("/")[1].strip()
    tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
    fig = plt.figure(figsize=(12, 8))
    ax = plt.gca()
    mod_waves.close()
    if len(ifos) > 1:
        lvls = [90]

    if psd is not None:
        ax2 = ax.twinx()

    for i, ifo in enumerate(ifos):
        mod_f = h5py.File(file, "r")
        mod_waves = mod_f[ifo]
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        flen = len(mod_waves["fd/freqs"])
        mod_amps = np.empty((pos_len, flen))
        x = np.empty(flen)
        mod_waves["fd/freqs"].read_direct(x)
        mod_waves["fd/mod_waves/amp"].read_direct(mod_amps)
        mod_amps = np.abs(mod_amps)
        if maxl:
            maxlamp = np.empty(flen)
            mod_waves["fd/maxl_wave/amp"].read_direct(maxlamp)
        mod_f.close()

        if no_mod_waves is not None:
            no_spline_waves = h5py.File(no_mod_waves, "r")
            nosp = no_spline_waves[ifo]
            flen_nosp = len(nosp["fd/freqs"])
            x_nosp = np.empty(flen_nosp)
            pos_len_nospline = nosp["fd/mod_waves/amp"].shape[0]
            mod_amps_nospline = np.empty((pos_len_nospline, flen_nosp))
            nosp["fd/mod_waves/amp"].read_direct(mod_amps_nospline)
            nosp["fd/freqs"].read_direct(x_nosp)
            no_spline_waves.close()
            amp_nospline = np.median(mod_amps_nospline, axis=0)

        amp = np.median(mod_amps, axis=0)
        if signal is not None:
            st = signal(ifo)
            ax.loglog(st.freqs, st.amp_fd, label="{}: Signal".format(st.ifo))
        if maxl:
            ax.loglog(x, maxlamp, label=f"{ifo} Max-logl waveform")
        if pos_samps:
            first = True
            iters = np.random.randint(0, pos_len, size=10)
            for i in iters:
                ax.loglog(x, mod_amps[i, :], color="g", alpha=0.3, label="posterior Draw" if first else None)
                first = False
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_amps, tmp, axis=0)
            high = np.percentile(mod_amps, 100 - tmp, axis=0)
            ax.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=0.35)
            if no_mod_waves is not None:
                low = np.percentile(mod_amps_nospline, tmp, axis=0)
                high = np.percentile(mod_amps_nospline, 100 - tmp, axis=0)
                ax.fill_between(x_nosp, high, low, color=COLOR_CYCLE[-(i + 1)], alpha=0.25)
        if no_mod_waves is not None:
            ax.loglog(x_nosp, (amp_nospline), color=COLOR_CYCLE[-(i + 1)], label=f"{ifo} Median Wave(nospline)")
        ax.loglog(x, (amp), color=COLOR_CYCLE[i], label="{} Median waveform".format(ifo))
        if psd is not None:
            ax2.loglog(psd.domain[ifo], psd.data[ifo], label=f"{ifo}_psd", alpha=0.6)
    if psd is not None:
        ax2.set_ylabel("Power Spectral Density")
        ax2.set_ylim(1e-49, 1e-29)
    plt.title(tit_prefix + "Plotted from file: {}".format(file) + "\n{} % Intervals shown".format(lvls))
    ax.set_xlabel("Freq (Hz)")
    ax.set_ylabel("Strain Amp(m)")
    ax.set_ylim(1e-26, 1e-22)
    if flow is None or fhigh is None:
        ax.set_xlim(min(x) if min(x) >= 10.0 else 10.0, max(x) + 100.0)
    else:
        ax.set_xlim(flow, fhigh)
    if grid:
        plt.grid()

    if psd is not None:
        lines, labels = ax.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2)
    else:
        plt.legend()

    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def plot_modified_waveform_amp_td(
    file,
    lvls=[50, 90, 95],
    filename=None,
    out_pref="",
    dpi=300,
    signal=None,
    show=False,
    zoom=False,
    log=False,
    no_mod_waves=None,
    ifos=["H1"],
):
    mod_waves = h5py.File(file, "r")
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    pref = file.split("/")[1].strip()
    tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
    fig = plt.figure(figsize=(12, 8))
    mod_waves.close()
    if len(ifos) > 1:
        lvls = [90]
    for i, ifo in enumerate(ifos):
        mod_f = h5py.File(file, "r")
        mod_waves = mod_f[ifo]
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        tlen = len(mod_waves["td/times"])
        mod_amps = np.empty((pos_len, tlen))
        x = np.empty(tlen)
        mod_waves["td/times"].read_direct(x)
        mod_waves["td/mod_waves/amp"].read_direct(mod_amps)
        mod_f.close()
        amp = np.median(mod_amps, axis=0)
        if no_mod_waves is not None:
            no_spline_waves = h5py.File(no_mod_waves, "r")
            nosp = no_spline_waves[ifo]
            pos_len_nospline = nosp["td/mod_waves/amp"].shape[0]
            mod_amps_nospline = np.empty((pos_len_nospline, tlen))
            nosp["td/mod_waves/amp"].read_direct(mod_amps_nospline)
            no_spline_waves.close()
            amp_nospline = np.median(mod_amps_nospline, axis=0)
        if signal is not None:
            for st in signal:
                plt.plot(
                    st.times - 4.0, st.amp_td, label="{}:{}-Frame Strain Data".format(st.ifo, st.prefix), alpha=0.75
                )
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_amps, tmp, axis=0)
            high = np.percentile(mod_amps, 100 - tmp, axis=0)
            plt.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=0.35)
            if no_mod_waves is not None:
                low = np.percentile(mod_amps_nospline, tmp, axis=0)
                high = np.percentile(mod_amps_nospline, 100 - tmp, axis=0)
                plt.fill_between(x, high, low, color=COLOR_CYCLE[-(i + 1)], alpha=0.25)
        if no_mod_waves is not None:
            plt.plot(x, np.abs(amp_nospline), color=COLOR_CYCLE[-(i + 1)], label="Median {} Wave(nospline)".format(ifo))
        plt.plot(x, np.abs(amp), color=COLOR_CYCLE[i], label="{} Median waveform".format(ifo))
    plt.legend()
    plt.xlabel("Time (sec)")
    plt.ylabel("Strain Amp(m)")
    plt.title(tit_prefix + "Plotted from file: {}\n {} CI plotted".format(file, lvls))
    if zoom:
        plt.xlim(-0.5, 0.1)
    if log:
        plt.yscale("log")
    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def plot_modified_waveform_phase_td(
    file,
    lvls=[50, 90, 95],
    filename=None,
    out_pref="",
    dpi=300,
    signal=None,
    show=False,
    zoom=False,
    no_mod_waves=None,
    ifos=["H1"],
):
    mod_waves = h5py.File(file, "r")
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    pref = file.split("/")[1].strip()
    tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
    fig = plt.figure(figsize=(12, 8))
    mod_waves.close()
    if len(ifos) > 1:
        lvls = [90]
    for c, ifo in enumerate(ifos):
        mod_f = h5py.File(file, "r")
        mod_waves = mod_f[ifo]
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        tlen = len(mod_waves["td/times"])
        x = np.empty(tlen)
        mod_waves["td/times"].read_direct(x)
        mod_phases = np.empty((pos_len, tlen))
        mod_waves["td/mod_waves/phase"].read_direct(mod_phases)
        mod_f.close()
        if no_mod_waves is not None:
            no_spline_waves = h5py.File(no_mod_waves, "r")
            nosp = no_spline_waves[ifo]
            pos_len_nospline = nosp["td/mod_waves/phase"].shape[0]
            mod_phases_nospline = np.empty((pos_len_nospline, tlen))
            nosp["td/mod_waves/phase"].read_direct(mod_phases_nospline)
            no_spline_waves.close()
            phase_nospline = np.median(mod_phases_nospline, axis=0)
        phase = np.median(mod_phases, axis=0)
        if zoom:
            ref_idx = np.arange(len(phase))[x > -0.25][0]
            phase -= phase[ref_idx]
            for i in range(mod_phases.shape[0]):
                ref_idx = np.arange(len(mod_phases[i, :]))[x > -0.25][0]
                mod_phases[i, :] -= mod_phases[i, :][ref_idx]
            if no_mod_waves is not None:
                for i in range(mod_phases_nospline.shape[0]):
                    ref_idx = np.arange(len(mod_phases_nospline[i, :]))[x > -0.25][0]
                    mod_phases_nospline[i, :] -= mod_phases_nospline[i, :][ref_idx]

        if signal is not None:
            for st in signal:
                plt.plot(st.times, st.phase_td, label="{}:{}-Frame Strain Data".format(st.ifo, st.prefix))
        m = 0
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_phases, tmp, axis=0)
            high = np.percentile(mod_phases, 100 - tmp, axis=0)
            plt.fill_between(x, high, low, color=COLOR_CYCLE[c], alpha=0.35)
            if no_mod_waves is not None:
                tmp = (100 - lvl) / 2
                low = np.percentile(mod_phases_nospline, tmp, axis=0)
                high = np.percentile(mod_phases_nospline, 100 - tmp, axis=0)
                plt.fill_between(x, high, low, color=COLOR_CYCLE[-(c + 1)], alpha=0.35)
            if max(high) > m:
                m = max(high)

        if no_mod_waves is not None:
            plt.plot(x, phase_nospline, color=COLOR_CYCLE[-(c + 1)], label=f"{ifo} Median_wf (nospline)")
        plt.plot(x, phase, color=COLOR_CYCLE[c], label=f"{ifo} Median_wf")
    plt.title(tit_prefix + "Plotted from file: {}".format(file))
    plt.xlabel("Time (sec)")
    plt.ylabel("Phase (deg)")
    if zoom:
        plt.xlim(-0.25, x[-1])
    plt.legend()
    plt.ylim(0, m + 5)
    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def plot_multiple_waveform_amp_fd(files, labels, filename=None, maxl=False, flow=None, fhigh=None):
    ifo = "H1"
    fig = plt.figure(figsize=(12, 10))
    ax = plt.gca()
    lvls = [90]
    for i, file, label in zip(range(len(files)), files, labels):
        modfile = h5py.File(file, "r")
        mod_waves = modfile[ifo]
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        flen = len(mod_waves["fd/freqs"])
        x = np.empty(flen)
        mod_amps = np.empty((pos_len, flen))
        mod_waves["fd/freqs"].read_direct(x)
        mod_waves["fd/mod_waves/amp"].read_direct(mod_amps)
        mod_amps = np.abs(mod_amps)
        if maxl:
            maxlamp = np.empty(flen)
            mod_waves["fd/maxl_wave/amp"].read_direct(maxlamp)
        modfile.close()
        amp = np.median(mod_amps, axis=0)
        if maxl:
            ax.loglog(x, maxlamp, color=COLOR_CYCLE[i])
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_amps, tmp, axis=0)
            high = np.percentile(mod_amps, 100 - tmp, axis=0)
            ax.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=0.35)
        ax.loglog(x, amp, color=COLOR_CYCLE[i], label=label)
    plt.grid()
    plt.legend()
    ax.set_xlabel("Freq (Hz)")
    ax.set_ylabel("Strain Amp(m)")
    if flow is None or fhigh is None:
        ax.set_xlim(min(x) if min(x) >= 10.0 else 10.0, max(x) + 100.0)
    else:
        ax.set_xlim(flow, fhigh)
    ax.set_ylim(1e-26, 1e-22)
    if filename is not None:
        plt.savefig(filename, dpi=400)


def plot_modified_waveform_phase_fd(
    file,
    lvls=[50, 90, 95],
    filename=None,
    out_pref="",
    dpi=300,
    signal=None,
    show=False,
    log=False,
    no_mod_waves=None,
    pos_samps=False,
    psd=None,
    grid=True,
    ifos=["H1"],
    sig_flow=10.0,
    flow=None,
    fhigh=None,
    maxl=False,
):
    mod_waves = h5py.File(file, "r")
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    pref = file.split("/")[1].strip()
    tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
    mod_waves.close()
    fig = plt.figure(figsize=(12, 8))
    ax = plt.gca()
    if len(ifos) > 1:
        lvls = [90]
    if psd is not None:
        ax2 = ax.twinx()
    for i, ifo in enumerate(ifos):
        mod_f = h5py.File(file, "r")
        mod_waves = mod_f[ifo]
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        flen = len(mod_waves["fd/freqs"])
        mod_phases = np.empty((pos_len, flen))
        x = np.empty(flen)
        mod_waves["fd/freqs"].read_direct(x)
        mod_waves["fd/mod_waves/phase"].read_direct(mod_phases)
        mod_phases *= 180 / np.pi
        if maxl:
            maxlphase = np.empty(flen)
            mod_waves["fd/maxl_wave/phase"].read_direct(maxlphase)
        mod_f.close()
        if no_mod_waves is not None:
            no_spline_waves = h5py.File(no_mod_waves, "r")
            nosp = no_spline_waves[ifo]
            flen_nosp = len(nosp["fd/freqs"])
            x_nosp = np.empty(flen_nosp)
            pos_len_nospline = nosp["fd/mod_waves/amp"].shape[0]
            mod_phases_nospline = np.empty((pos_len_nospline, flen_nosp))
            nosp["fd/mod_waves/phase"].read_direct(mod_phases_nospline)
            nosp["fd/freqs"].read_direct(x_nosp)
            no_spline_waves.close()
            mod_phases_nospline *= 180 / np.pi
            phase_nospline = np.median(mod_phases_nospline, axis=0)
        phase = np.median(mod_phases, axis=0)
        max_post_phase = max(phase)
        if signal is not None:
            st = signal(ifo)
            freqs = st.freqs[st.freqs > sig_flow]
            phase_fd = st.phase_fd[st.freqs > sig_flow]
            phase_fd -= phase_fd[0]
            phase_fd *= 180 / np.pi
            max_selector = freqs > 40.0
            max_sig_phase = max(phase_fd[max_selector])
            phase_offset = max_post_phase - max_sig_phase
            ax.plot(freqs, phase_fd + phase_offset, label="{}: Signal".format(st.ifo))
        if maxl:
            ax.plot(x, maxlphase * 180 / np.pi, label=f"{ifo} Max-logl waveform")
        if pos_samps:
            first = True
            for i in np.random.randint(0, pos_len, size=10):
                ax.plot(x, mod_phases[i, :], color="g", alpha=0.3, label="posterior draws" if first else None)
                first = False
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_phases, tmp, axis=0)
            high = np.percentile(mod_phases, 100 - tmp, axis=0)
            ax.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=0.35)
            if no_mod_waves is not None:
                low = np.percentile(mod_phases_nospline, tmp, axis=0)
                high = np.percentile(mod_phases_nospline, 100 - tmp, axis=0)
                ax.fill_between(x_nosp, high, low, color=COLOR_CYCLE[-(i + 1)], alpha=0.35)
        if no_mod_waves is not None:
            ax.plot(x_nosp, phase_nospline, color=COLOR_CYCLE[-(i + 1)], label=f"{ifo} Median waveform(nospline)")
        ax.plot(x, phase, color=COLOR_CYCLE[i], label="{} Median waveform".format(ifo))
        if psd is not None:
            ax2.loglog(psd.domain[ifo], psd.data[ifo], label=f"{ifo}_psd", alpha=0.6)

    if psd is not None:
        ax2.set_ylim(1e-49, 1e-29)
        ax2.set_ylabel("Power Spectral Density")
    ax.set_xlabel("Frequency (Hz) ")
    ax.set_ylabel("Phase (Degrees)")
    if flow is None or fhigh is None:
        ax.set_xlim(min(x) if min(x) >= 10.0 else 10.0, max(x) + 100.0)
    else:
        ax.set_xlim(flow, fhigh)
    if log:
        plt.xscale("log")
    if grid:
        plt.grid()

    if psd is not None:
        lines, labels = ax.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2)
    else:
        plt.legend()

    plt.title(tit_prefix + "Plotted from file: {}".format(file) + "\n{} % Intervals shown".format(lvls))
    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def plot_multiple_waveform_phase_td(
    files, filename=None, dpi=300, out_pref="", signal=None, show=False, zoom=False, lvls=[75, 85, 95]
):

    n_waves = len(files)
    fig = plt.figure(figsize=(12, 8))
    title_str = ""
    for i, file in enumerate(files):
        mod_waves = h5py.File(file, "r")
        ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
        pref = file.split("/")[1].strip()
        tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
        pos_len = mod_waves["fd/mod_waves/amp"].shape[0]
        tlen = len(mod_waves["/td/times"])
        mod_phases = np.empty((pos_len, tlen))
        x = np.empty(tlen)
        mod_waves["/td/times"].read_direct(x)
        mod_waves["/td/mod_waves/phase"].read_direct(mod_phases)
        mod_waves.close()
        phase = np.median(mod_phases, axis=0)
        if zoom:
            ref_idx = np.arange(len(phase))[x > -0.25][0]
            phase -= phase[ref_idx]
            for j in range(mod_phases.shape[0]):
                ref_idx = np.arange(len(mod_phases[j, :]))[x > -0.25][0]
                mod_phases[j, :] -= mod_phases[j, :][ref_idx]
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_phases, tmp, axis=0)
            high = np.percentile(mod_phases, 100 - tmp, axis=0)
            plt.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=1 / (n_waves + len(lvls)))
        del mod_phases, mod_waves
        plt.plot(x, phase, label="Median_wf\n" + tit_prefix, color=COLOR_CYCLE[i])
        title_str += tit_prefix + "\n"
    if signal is not None:
        for st in signal:
            plt.loglog(st.times, st.phase_td, label="{}:{}-Frame Strain Data".format(st.ifo, st.prefix))
    plt.legend()
    title_app = title_str
    for i, j in enumerate(lvls):
        title_app += str(j) + ": "
    plt.title(title_app + "Percentiles Shown:")
    plt.xlabel("Time(sec)")
    plt.ylabel("Phase (Radians)")
    if zoom:
        plt.xlim(-0.25, x[-1])
        plt.ylim(0, max(high))

    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def plot_multiple_waveform_phase_fd(files, labels, filename=None, maxl=False, flow=None, fhigh=None):
    ifo = "H1"
    fig = plt.figure(figsize=(12, 10))
    ax = plt.gca()
    lvls = [90]
    for i, file, label in zip(range(len(files)), files, labels):
        modfile = h5py.File(file, "r")
        mod_waves = modfile[ifo]
        pos_len = mod_waves["fd/mod_waves/phase"].shape[0]
        flen = len(mod_waves["fd/freqs"])
        x = np.empty(flen)
        mod_phase = np.empty((pos_len, flen))
        mod_waves["fd/freqs"].read_direct(x)
        mod_waves["fd/mod_waves/phase"].read_direct(mod_phase)
        mod_phase *= 180 / np.pi
        if maxl:
            maxlphase = np.empty(flen)
            mod_waves["fd/maxl_wave/phase"].read_direct(maxlphase)
        modfile.close()
        phase = np.median(mod_phase, axis=0)
        if maxl:
            ax.plot(x, maxlphase * 180 / np.pi, color=COLOR_CYCLE[i])
        for lvl in lvls:
            tmp = (100 - lvl) / 2
            low = np.percentile(mod_phase, tmp, axis=0)
            high = np.percentile(mod_phase, 100 - tmp, axis=0)
            ax.fill_between(x, high, low, color=COLOR_CYCLE[i], alpha=0.35)
        ax.plot(x, phase, color=COLOR_CYCLE[i], label=label)
    plt.grid()
    plt.legend()
    ax.set_xlabel("Frequency (Hz) ")
    ax.set_ylabel("Phase (Degrees)")
    if flow is None or fhigh is None:
        ax.set_xlim(min(x) if min(x) >= 10.0 else 10.0, max(x) + 100.0)
    else:
        ax.set_xlim(flow, fhigh)
    ax.set_xscale("log")
    if filename is not None:
        plt.savefig(filename, dpi=400)


def plot_multiple_density(
    files,
    prefs,
    keys,
    cumulative=False,
    title=True,
    truth=None,
    filename=None,
    bins=50,
    label_fontsize=16,
    title_fontsize=16,
    quantiles=[0.1, 0.9],
    dpi=300,
    density=True,
    show=False,
    verbose=False,
    burn=True,
    man_burn=None,
    GW=False,
    inj_params=None,
):
    naxes = len(keys)
    quants = (quantiles[1] - quantiles[0]) * 100
    fig, axs = plt.subplots(ncols=2, nrows=naxes, figsize=(12, 10))
    verbose_output = ""
    if truth is not None:
        truth_plotted = False

    for file, pr, j in zip(files, prefs, np.arange(len(files))):
        if file.split(".")[-1] == "json":
            _dat_file = open(file, "r")
            pos = json.load(_dat_file)
            posterior = get_post_from_json(pos)
        else:
            posterior = h5py.File(file, "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
            if burn:
                burnin = get_burn(posterior["cycle"])
                if burnin < 0.95 * len(posterior):
                    posterior = posterior[int(burnin) :]
        if man_burn is not None:
            if isinstance(man_burn, list):
                if len(man_burn) == len(files):
                    posterior = posterior[int(man_burn[j]) :]
                else:
                    raise TypeError("Manual burn as a list must equal length of files list")
            else:
                posterior = posterior[man_burn:]
        for k in keys:
            try:
                _ = posterior[k]
            except KeyError:
                print(
                    "File: {0} for dataset {1} does not have posterior samples for parameter: {2}".format(file, pr, k)
                )
                del posterior
                continue
        for key, ax, i in zip(keys, axs, np.arange(len(keys))):
            ax[0].hist(
                posterior[key],
                bins=bins,
                density=density,
                alpha=0.3,
                cumulative=cumulative,
                color=COLOR_CYCLE[j],
                label=pr,
            )
            ax[0].set_xlabel(key, fontsize=label_fontsize)
            ax[0].set_ylabel("Density" if density else "Freqeuncy", fontsize=label_fontsize)
            if truth is not None and len(truth) == len(keys):
                if truth[i] == "None" or truth[i] == None:
                    truth[i] = None
                else:
                    truth[i] = float(truth[i])
                if truth[i] is not None:
                    if file == files[0]:
                        ax[0].axvline(
                            truth[i], ls="-", color="g", label="Injected Value" if not GW else "Median_event_value"
                        )
            summary = get_one_dimensional_median_and_error_bar(key, posterior, quantiles=quantiles)
            if verbose:
                verbose_output += "Run: {0} for key: {1} {2} Quantiles: {3} \n".format(pr, key, quants, summary.string)
            ax[0].axvline(summary.median - summary.minus, ls="--", color=COLOR_CYCLE[j])
            ax[0].axvline(summary.median + summary.plus, ls="--", color=COLOR_CYCLE[j])
            ax[1].plot(posterior[key], color=COLOR_CYCLE[j], alpha=0.5, label=pr)
            ax[1].set_ylabel(key, fontsize=label_fontsize)
            ax[1].set_xlabel("Iteration", fontsize=label_fontsize)
            if truth is not None and len(truth) == len(keys):
                if truth[i] == "None" or truth[i] == None:
                    truth[i] = None
                else:
                    truth[i] = float(truth[i])
                if truth[i] is not None:
                    if file == files[0]:
                        ax[1].axhline(
                            truth[i], ls="-", color="g", label="Injected Value" if not GW else "Median_event_value"
                        )
            ax[1].axhline(summary.median - summary.minus, ls="--", color=COLOR_CYCLE[j])
            ax[1].axhline(summary.median + summary.plus, ls="--", color=COLOR_CYCLE[j])

            ax[0].legend()
            ax[1].legend()
    file_str = "Files: "

    for f in files:
        file_str += f + " \n"
    plt.suptitle(
        "{0} Posteriors:  {1}% Quantiles  shown:\n".format(keys, quants) + "Files: " + file_str, fontsize=title_fontsize
    )
    plt.subplots_adjust(hspace=0.5, top=0.8)

    plt.legend()
    if filename is not None:
        for k in keys:
            filename += "_" + str(k)
        if cumulative:
            file_name = filename + "_cdf.png"
        else:
            file_name = filename + "_pdf.png"
        fig.savefig(file_name, dpi=dpi)
    if verbose:
        print(verbose_output)
    if show:
        plt.show()
    return fig


def get_one_dimensional_median_and_error_bar(key, post, quantiles=[0.05, 0.95], fmt=".2f"):
    summary = namedtuple("summary", ["median", "lower", "upper", "string"])

    if len(quantiles) != 2:
        raise ValueError("quantiles must be of length 2")

    quants_to_compute = np.array([quantiles[0], 0.5, quantiles[1]])
    quants = np.percentile(post[key], quants_to_compute * 100)
    summary.median = quants[1]
    summary.plus = quants[2] - summary.median
    summary.minus = summary.median - quants[0]
    fmt = "{{0:{0}}}".format(fmt).format
    string_template = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
    summary.string = string_template.format(fmt(summary.median), fmt(summary.minus), fmt(summary.plus))
    return summary


def plot_hist_trace_posteriors(
    files,
    prefs,
    key,
    cumulative=False,
    title=True,
    truth=None,
    filename=None,
    bins=50,
    label_fontsize=16,
    title_fontsize=16,
    quantiles=[0.05, 0.95],
    dpi=300,
    density=True,
    show=False,
    out_pref="",
    burn=True,
):
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(15, 5),)
    title_str = ""
    quants = quantiles[1] - quantiles[0]
    for file, pref, j in zip(files, prefs, np.arange(len(files))):
        posterior = h5py.File(file, "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
        if burn:
            burnin = get_burn(posterior["cycle"])
            posterior = posterior[int(burnin + 750) :]
        try:
            _ = posterior[key]
        except KeyError:
            print(
                "File: {0} for dataset {1} does not have posterior samples for parameter: {2}".format(file, pref, key)
            )
            posterior.close()
            continue
        axs[0].hist(
            posterior[key],
            bins=bins,
            alpha=0.4,
            label=pref,
            density=density,
            cumulative=cumulative,
            color=COLOR_CYCLE[j],
        )
        axs[1].plot(posterior[key], label=pref, alpha=0.6, color=COLOR_CYCLE[j])
        if truth is not None:
            axs[0].axvline(truth, color="m", label="truth" if j == len(files) - 1 else None)

        summary = get_one_dimensional_median_and_error_bar(key, posterior, quantiles=quantiles)
        axs[0].axvline(summary.median - summary.minus, ls="--", color=COLOR_CYCLE[j])
        axs[0].axvline(summary.median + summary.plus, ls="--", color=COLOR_CYCLE[j])
        if title:
            axs[0].set_title(key + ": " + summary.string, fontsize=title_fontsize)

        axs[1].set_ylabel(key, fontsize=label_fontsize)
        axs[1].set_xlabel("Iteration", fontsize=label_fontsize)
        if truth is not None:
            axs[1].axhline(truth, color="m", label="truth" if j == len(files) - 1 else None)
        axs[1].axhline(summary.median - summary.minus, ls="--", color=COLOR_CYCLE[j])
        axs[1].axhline(summary.median + summary.plus, ls="--", color=COLOR_CYCLE[j])
        if title:
            axs[1].set_title(key + ": " + summary.string, fontsize=title_fontsize)

        axs[0].set_xlabel(key, fontsize=label_fontsize)
        axs[0].set_ylabel("Density" if density else "Freqeuncy", fontsize=label_fontsize)
        axs[1].set_xlabel("Iteration", fontsize=label_fontsize)
        axs[1].set_ylabel(key)
        axs[1].legend()
        axs[0].legend()

        title_str += file + "\n"
        del posterior
    plt.suptitle(
        "{0} Posteriors\n {1} Quantiles shown:\n".format(key, int(quants * 100)) + "Files: " + title_str,
        fontsize=title_fontsize,
    )
    plt.subplots_adjust(top=0.5)

    if filename is not None:
        if cumulative:
            file_name = filename + key + "_cdf"
        else:
            file_name = filename + key + "_pdf"
        outdir = out_pref + file_name
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()

    return fig


def plot_splines(
    file, filename=None, dpi=300, out_pref="", show=False, zoom={}, pos_samps=False, frange=None, ifos=None,
):
    if ifos is not None:
        ifo = ifos[0]
    else:
        ifo = "H1"
    mod_waves = h5py.File(file, "r")
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    nodes = mod_waves.attrs["nodes"]
    pref = file.split("/")[1].strip()
    tit_prefix = "{0}_PTMCMC RUN: {1}\n".format(pref, ptmcmc_file)
    pos_len = mod_waves["/{}/fd/sigF".format(ifo)].shape[0]
    flen = len(mod_waves["/{}/fd/freqs".format(ifo)])
    phis = np.empty((pos_len, flen), dtype=np.complex128)
    amps = np.empty_like(phis, dtype=np.float64)
    x = np.empty(flen)
    logfs = np.empty((pos_len, nodes))
    mod_waves["/{}/fd/freqs".format(ifo)].read_direct(x)
    mod_waves["/{}/fd/amps".format(ifo)].read_direct(amps)
    mod_waves["/{}/fd/phis".format(ifo)].read_direct(phis)
    mod_waves["/{}/fd/logfs".format(ifo)].read_direct(logfs)
    mod_waves.close()

    unwrapped_angles = np.unwrap(np.angle(phis))
    del phis
    mean_amps = np.median(amps, axis=0)
    mean_angles = np.median(unwrapped_angles, axis=0)
    fig, axs = plt.subplots(nrows=2, ncols=1, sharex="all")
    ax = axs[0]
    ax.plot(x, mean_amps * 100, "r", label="Median Spline", zorder=10)
    for i in range(nodes):
        ax.axvline(np.exp(np.median(logfs[:, i])), color="b", label="Median Node Postitions" if i == 0 else None)
    for lvl in [80, 90]:
        tmp = (100 - lvl) / 2.0
        low = np.percentile(amps, tmp, axis=0) * 100
        high = np.percentile(amps, 100 - tmp, axis=0) * 100
        ax.fill_between(x, low, high, color="k", alpha=0.0035 * lvl, lw=0, label="{}% CI".format(lvl))
    if pos_samps:
        First = True
        for i in np.random.randint(0, pos_len, size=5):
            ax.plot(x, amps[i, :] * 100, color="g", alpha=0.3, label="Posterior Draws" if First else None)
            a = [get_near(logfs[i, j], amps[i, :], x) * 100 for j in range(nodes)]
            ax.scatter(np.exp(logfs[i, :]), a, color="k", linewidths=0.1, marker="X")
            First = False

    del amps
    ax.axhline(0, color="m", ls="--", label="Y-axis")
    ax.set_xscale("log")
    if zoom:
        ax.set_ylim((zoom["amp_low"], zoom["amp_high"]))
    ax.set_ylabel(r"$\delta A$  (%)")
    ax = axs[1]
    ax.plot(x, mean_angles * 180 / np.pi, "r", label="Median Spline", zorder=10)
    for i in range(nodes):
        ax.axvline(np.exp(np.median(logfs[:, i])), color="b", label="Median Node Postitions" if i == 0 else None)
    for lvl in [80, 90]:
        tmp = (100 - lvl) / 2.0
        low = np.percentile(unwrapped_angles, tmp, axis=0) * 180 / np.pi
        high = np.percentile(unwrapped_angles, 100 - tmp, axis=0) * 180 / np.pi
        ax.fill_between(x, low, high, color="k", alpha=0.0035 * lvl, lw=0, label="{}% CI".format(lvl))
    if pos_samps:
        First = True
        for i in np.random.randint(0, pos_len, size=5):
            ax.plot(
                x,
                unwrapped_angles[i, :] * 180 / np.pi,
                color="g",
                alpha=0.3,
                label="Posterior Draws" if First else None,
            )
            p = [get_near(logfs[i, j], unwrapped_angles[i, :], x) * 180 / np.pi for j in range(nodes)]
            ax.scatter(np.exp(logfs[i, :]), p, color="k", linewidths=0.1, marker="X")
            First = False
    del unwrapped_angles
    ax.axhline(0, color="m", ls="--", label="Y-axis")
    ax.set_xscale("log")
    if zoom:
        ax.set_ylim((zoom["phase_low"], zoom["phase_high"]))
    ax.set_xlabel("Frequency (Hz)")
    ax.set_ylabel(r"$\delta \phi$ (Deg)")
    ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.22), fancybox=True, shadow=True, ncol=5)
    if frange is None:
        ax.set_xlim(39.5, np.max(np.exp(np.median(logfs, axis=0))) + 5)
    else:
        ax.set_xlim(frange)
    title_str = tit_prefix + "Plotted from file: {}".format(file)
    axs[0].set_title(r"$\delta A$ Spline Interpolation")
    axs[1].set_title(r"$\delta \phi$ Spline Interpolation")
    # fig.suptitle("%s Nodes Spsig: %s \n %s" % (nodes, ptmcmc_file, title_str))
    fig.set_size_inches(10, 8)
    if filename is not None:
        outdir = out_pref + filename
        plt.savefig(outdir, dpi=dpi)
    if show:
        plt.show()
    return fig


def compare_splines(files, filename=None, dpi=300, show=False, labels=None, zoom={}):
    fig, axs = plt.subplots(nrows=2, ncols=1, sharex="all")
    if len(files) > len(COLOR_CYCLE):
        raise ValueError("Comparison only supports max 9 files currently:")
    use_labels = False if labels is None else True
    # plot settings and strings
    ax = axs[0]
    ax.set_xscale("log")
    ax.set_ylabel(r"$\delta A$  (%)")
    ax = axs[1]
    ax.set_xscale("log")
    ax.set_xlabel("Frequency (Hz)")
    ax.set_ylabel(r"$\delta \phi$ (Deg)")
    flow = np.inf
    fhigh = 0
    for f, file in enumerate(files):
        spline_dict = generate_spline_factors(file, burn=True)
        nodes = spline_dict["logfs"].shape[1]
        axs[0].set_title("Amplitude")
        axs[1].set_title("Phase")
        pref = file.split("/")[1].strip()
        unwrapped_angles = np.unwrap(np.angle(spline_dict["phis"]))
        amps = spline_dict["amps"]
        logfs = spline_dict["logfs"]
        mean_nodes = np.exp(np.median(logfs, axis=0))
        x = spline_dict["freqs"]
        del spline_dict
        if min(mean_nodes) < flow:
            flow = min(mean_nodes)
        if max(mean_nodes) > fhigh:
            fhigh = max(mean_nodes)
        mean_amps = np.mean(amps, axis=0)
        mean_angles = np.mean(unwrapped_angles, axis=0)
        ax = axs[0]
        ax.plot(x, mean_amps * 100, COLOR_CYCLE[f])

        # for i in range(nodes):
        #   ax.axvline(mean_nodes[i], color=COLOR_CYCLE[f], label='Mean Node' if i==1 and f==0 else None)
        for lvl in [90]:
            tmp = (100 - lvl) / 2.0
            if use_labels and lvl == 90:
                leg = labels[f]
            elif not use_labels and lvl == 90:
                leg = pref
            else:
                leg = None
            low = np.percentile(amps, tmp, axis=0) * 100
            high = np.percentile(amps, 100 - tmp, axis=0) * 100
            ax.fill_between(x, low, high, color=COLOR_CYCLE[f], alpha=0.24, lw=3, label=leg)
        del amps
        # ax.axhline(5, color='b', ls='--')#, label='Prior Widths')
        # ax.axhline(-5, color='b', ls='--')
        ax = axs[1]
        # ax.axhline(5, color='b', ls='--')#, label='Prior Widths')
        # ax.axhline(-5, color='b', ls='--')
        ax.plot(x, mean_angles * 180 / np.pi, COLOR_CYCLE[f])
        # for i in range(nodes):
        #    ax.axvline(mean_nodes[i], color=COLOR_CYCLE[f], label='Mean Node' if i==1 and f==0 else None)
        for lvl in [90]:
            tmp = (100 - lvl) / 2.0
            if use_labels and lvl == 90:
                leg = labels[f]
            elif not use_labels and lvl == 90:
                leg = pref
            else:
                leg = None
            low = np.percentile(unwrapped_angles, tmp, axis=0) * 180 / np.pi
            high = np.percentile(unwrapped_angles, 100 - tmp, axis=0) * 180 / np.pi
            ax.fill_between(x, low, high, color=COLOR_CYCLE[f], alpha=0.24, lw=3, label=leg)
            # ax.loglog(x, low, color=COLOR_CYCLE[f])
        del unwrapped_angles
    # axs[0].legend()
    ax.legend(loc="upper center", bbox_to_anchor=(0.5, -0.17), fancybox=True, shadow=True, ncol=5)
    ax.set_xlim(flow, fhigh)
    ax.axhline(0, color="m", ls="--")
    axs[0].axhline(0, color="m", ls="--")
    if zoom:
        axs[0].set_ylim((zoom["amp_low"], zoom["amp_high"]))
    if zoom:
        axs[1].set_ylim((zoom["phase_low"], zoom["phase_high"]))
    fig.suptitle("Spline Comparison Plot for files: {}".format(labels))
    fig.set_size_inches(10, 10)
    plt.tight_layout()
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
    if show:
        plt.show()
    return fig


def animate_reconstruct(
    modfile,
    filename=None,
    ifos=None,
    frange=(30.0, 2048.0),
    NFRAMES=100,
    progress=False,
    logger=EmptyLogger(),
    srate=4096.0,
    avg=None,
):

    fig = plt.figure(figsize=(14, 10), constrained_layout=True)
    nifos = len(ifos)
    gs = GridSpec(4, nifos, figure=fig)
    spline_amp_ax = fig.add_subplot(gs[2, :])
    spline_amp_ax.set_xscale("log")
    spline_amp_ax.set_xlabel("Frequency (Hz)")
    spline_amp_ax.set_ylabel(r"$\delta A$")
    spline_amp_ax.set_ylim((-60, 60))
    spline_amp_ax.set_xlim(frange)
    spline_phase_ax = fig.add_subplot(gs[3, :])
    spline_phase_ax.set_xscale("log")
    spline_phase_ax.set_xlabel("Frequency (Hz)")
    spline_phase_ax.set_ylabel(r"$\delta \phi$")
    spline_phase_ax.set_ylim((-100, 100))
    spline_phase_ax.set_xlim(frange)
    mod_waves = h5py.File(modfile, "r")
    det = ifos[0]
    pos_len = mod_waves[f"/{det}/fd/mod_waves/amp"].shape[0]
    skip = int(pos_len / NFRAMES)
    ptmcmc_file = mod_waves.attrs["PTMCMC_File"]
    pref = modfile.split("/")[1].strip()
    tit_prefix = "{0}\n Run File: {1}".format(pref, ptmcmc_file)
    fig.suptitle(f"Animation: \n {tit_prefix}")
    if avg is not None:
        indices = range(1 + avg, pos_len, skip)
        if len(indices) < 10:
            logger.info(f"not enough samples to create running average animation with avg = {avg}")
            return
        numpy_func = np.mean
    else:
        indices = range(1, pos_len, skip)
        numpy_func = np.median
    lvls = [85, 95]
    amp_ax = {}
    phase_ax = {}
    if srate < 2048.0:
        rec_fhigh = 512.0
    else:
        rec_fhigh = 1024.0
    for i, ifo in enumerate(ifos):
        amp_ax[ifo] = fig.add_subplot(gs[0, i])
        amp_ax[ifo].set_xscale("log")
        amp_ax[ifo].set_xlabel("Frequency (Hz)")
        amp_ax[ifo].set_ylabel("Amplitude")
        amp_ax[ifo].set_xlim((30.0, rec_fhigh))
        amp_ax[ifo].set_title(f"{ifo} Amplitude reconstruction")
        amp_ax[ifo].set_ylim((1e-28, 1e-21))
        phase_ax[ifo] = fig.add_subplot(gs[1, i])
        phase_ax[ifo].set_xscale("log")
        phase_ax[ifo].set_xlabel("Frequency (Hz)")
        phase_ax[ifo].set_ylabel("Phase (degrees)")
        phase_ax[ifo].set_xlim((30.0, rec_fhigh))
        phase_ax[ifo].set_title(f"{ifo} Phase reconstruction")
    cam = Camera(fig)
    logger.debug(f"Creating {len(indices)} frames for gif creation to save at {filename}")
    with progress_bar(progress, len(indices)) as prg_bar:
        for i in indices:
            if avg is not None:
                start = int(i - avg)
            else:
                start = 0
            for ifo in ifos:
                x = mod_waves[f"{ifo}/fd/freqs"]
                running_med = np.exp(numpy_func(mod_waves[f"{ifo}/fd/logfs"][start:i, :], axis=0))
                for lvl in lvls:
                    tmp = (100 - lvl) / 2
                    low = np.percentile(mod_waves[f"{ifo}/fd/mod_waves/amp"][start:i, :], tmp, axis=0)
                    high = np.percentile(mod_waves[f"{ifo}/fd/mod_waves/amp"][start:i, :], 100 - tmp, axis=0)
                    amp_ax[ifo].fill_between(x, high, low, color="k", alpha=0.35)
                    low = np.percentile(mod_waves[f"{ifo}/fd/mod_waves/phase"][start:i, :] * 180 / np.pi, tmp, axis=0)
                    high = np.percentile(
                        mod_waves[f"{ifo}/fd/mod_waves/phase"][start:i, :] * 180 / np.pi, 100 - tmp, axis=0
                    )
                    phase_ax[ifo].fill_between(x, high, low, color="k", alpha=0.35)
                for j in running_med:
                    amp_ax[ifo].axvline(j, color="r")
                    phase_ax[ifo].axvline(j, color="r")
                amp_ax[ifo].loglog(
                    x,
                    numpy_func(mod_waves[f"{ifo}/fd/mod_waves/amp"][start:i, :], axis=0),
                    color="k",
                    label="{} Median waveform".format(ifo),
                )
                phase_ax[ifo].semilogx(
                    x,
                    numpy_func(mod_waves[f"{ifo}/fd/mod_waves/phase"][start:i, :] * 180 / np.pi, axis=0),
                    color="k",
                    label="{} Median waveform".format(ifo),
                )
            running_med = np.exp(numpy_func(mod_waves[f"{ifos[0]}/fd/logfs"][start:i, :], axis=0))
            running_amp_mean = numpy_func(mod_waves[f"{ifos[0]}/fd/amps"][start:i, :], axis=0)
            unwrapped_angles = np.unwrap(np.angle(mod_waves[f"{ifos[0]}/fd/phis"]))
            running_phase_mean = numpy_func(unwrapped_angles[start:i, :], axis=0)
            ax = spline_amp_ax
            for j in running_med:
                ax.axvline(j, color="r")
                ax = spline_phase_ax
                ax.axvline(j, color="r")
                ax = spline_amp_ax
            ax.axhline(0, color="m", ls="--")
            ax.plot(x, running_amp_mean * 100, "k")
            for lvl in lvls:
                tmp = (100 - lvl) / 2.0
                low = np.percentile(mod_waves[f"{ifos[0]}/fd/amps"][start:i, :], tmp, axis=0) * 100
                high = np.percentile(mod_waves[f"{ifos[0]}/fd/amps"][start:i, :], 100 - tmp, axis=0) * 100
                ax.fill_between(x, low, high, color="k", alpha=0.3, lw=0)
                ax = spline_phase_ax
                low = np.percentile(unwrapped_angles[start:i, :], tmp, axis=0) * 180 / np.pi
                high = np.percentile(unwrapped_angles[start:i, :], 100 - tmp, axis=0) * 180 / np.pi
                ax.fill_between(x, low, high, color="k", alpha=0.3, lw=0)
                ax = spline_amp_ax
            ax = spline_phase_ax
            ax.axhline(0, color="m", ls="--")
            ax.plot(x, running_phase_mean * 180 / np.pi, "k")
            cam.snap()
            prg_bar.update(1)
        logger.debug("Animating snapped camera frames")
        animation = cam.animate()
        logger.info("Saving Animation")
        animation.save(filename, writer="pillow")


def plot_corners(files, labels, manburn=None, filename=None, show=False, burn=True, truth=False):
    INJECTED_PARAMS = {
        "mass1": 36,
        "mass2": 29,
        "distance": 450,
        "a_spin1": 0.0,
        "a_spin2": 0.0,
        "time": 16.0,
        "rightascension": 1.37,
        "declination": -1.26,
        "polarisation": 2.76,
    }
    PYCBC_WAVEFORM_PARAMS = ["mass1", "mass2", "distance", "spin1z", "spin2z", "tc", "ra", "dec", "polarization"]

    lines = []
    lastfig = None
    for file, pr, i in zip(files, labels, np.arange(len(files))):
        if file.split(".")[-1] == "json":
            _dat_file = open(file, "r")
            pos = json.load(_dat_file)
            posterior = get_post_from_json(pos)
        else:
            posfile = h5py.File(file, "r")
            posterior = posfile["lalinference"]["lalinference_mcmc"]["posterior_samples"]
            if burn:
                burnin = get_burn(posterior["cycle"])
                if burnin < 0.95 * len(posterior):
                    posterior = posterior[int(burnin) :]
        if manburn is not None:
            if isinstance(manburn, list):
                if len(manburn) == len(files):
                    posterior = posterior[int(manburn[i]) :]
                else:
                    raise TypeError("Manual burn as a list must equal length of files list")
            else:
                posterior = posterior[manburn:]
        plot_post = {
            "mass1": mass1_from_mchirp_q(posterior["chirpmass"], 1.0 / posterior["q"]),
            "mass2": mass2_from_mchirp_q(posterior["chirpmass"], 1.0 / posterior["q"]),
            "distance": np.exp(posterior["logdistance"]),
            "spin1z": posterior["a_spin1"],
            "spin2z": posterior["a_spin2"],
            "time": posterior["time"],
            "rightascension": posterior["rightascension"],
            "declination": posterior["declination"],
            "polarisation": posterior["polarisation"],
        }
        postarr = np.column_stack([plot_post[p] for p in plot_post.keys()])
        posfile.close()
        lines.append(mlines.Line2D([], [], color=COLOR_CYCLE[i], label=pr))
        lastfig = corner.corner(
            postarr,
            show_titles=True,
            labels=PYCBC_WAVEFORM_PARAMS,
            truths=list(INJECTED_PARAMS.values()) if truth else None,
            color=COLOR_CYCLE[i],
            truth_color="g",
            hist_kwargs={"density": True, "histtype": "stepfilled", "alpha": 0.6},
            density=True,
            fig=lastfig,
        )

    plt.legend(handles=lines, bbox_to_anchor=(-4.0, 6.0, 1.0, 0.0), fontsize="large", markerscale=6)
    plt.suptitle("Modified Waveform", fontsize=20)
    if show:
        plt.show()
    if filename is not None:
        plt.savefig(filename, dpi=400)
    return lastfig
