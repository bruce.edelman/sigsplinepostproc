import numpy as np
import matplotlib.pyplot as plt

COLOR_CYCLE = ["#332288", "#CC6677", "#88CCEE", "#DDCC77", "#44AA99", "#117733", "#882255", "#999933", "#AA4499"]
# shuffle(COLOR_CYCLE)
COLOR_CYCLE = COLOR_CYCLE[::-1]


class Psd(object):
    def __init__(self, paths, ifos, asd=False):
        self.domain = {}
        self.data = {}
        for ifo, path in zip(ifos, paths):
            self.domain[ifo], self.data[ifo] = self.get_from_file(path)
            if asd:
                self.data[ifo] = self.data[ifo] ** 2

    @staticmethod
    def get_from_file(path):
        f, sn = np.genfromtxt(path, unpack=True)
        return f, sn

    def plot(self, show=False, filename=None, dpi=300, lowf=None, highf=None):
        fig = plt.figure(figsize=(10, 8))
        for i, ifo in enumerate(self.domain.keys()):
            plt.loglog(self.domain[ifo], self.data[ifo], label=ifo, alpha=0.8, color=COLOR_CYCLE[i])
        if lowf is not None and highf is not None:
            plt.xlim(lowf, highf)
        plt.ylim(1e-49, 1e-29)
        plt.grid()
        plt.legend()
        if show:
            plt.show()
        if filename is not None:
            plt.savefig(filename, dpi=dpi)
        return fig
