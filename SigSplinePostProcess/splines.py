import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import CubicSpline
import h5py
from .pbar import progress_bar
from .utils import EmptyLogger
from celluloid import Camera
from random import shuffle

COLOR_CYCLE = ["#332288", "#CC6677", "#88CCEE", "#DDCC77", "#44AA99", "#117733", "#882255", "#999933", "#AA4499"]
# shuffle(COLOR_CYCLE)
COLOR_CYCLE = COLOR_CYCLE[::-1]


def generate_spline_factors(posterior, attrs):
    flow = attrs["flow"]
    srate = attrs["sampleRate"]
    seglen = attrs["segmentLength"]
    nnodes = attrs["spsig_npts"]
    N = int(float(srate) * float(seglen) / 2 + 1)
    dF = 1.0 / float(seglen)
    freqs = np.arange(N) * dF
    fhigh = float(srate) / 2
    pos_len = len(posterior)
    thin = 1
    if pos_len > 25000:
        thin = int(np.ceil(pos_len / 25000))
    idxs = range(0, pos_len, thin)
    pos_len = len(idxs)
    phis = np.ones((pos_len, len(freqs)), dtype=np.complex)
    amps = np.zeros_like(phis, dtype=np.float)
    sigF = np.empty_like(phis, dtype=np.complex)
    logfs = np.zeros((pos_len, nnodes), dtype=np.float)
    k = 0
    for idx in idxs:
        samp = posterior[idx]
        amp_nodes = np.zeros(nnodes)
        lfreqs = np.zeros(nnodes)
        dA = np.empty(len(freqs))
        dPhi = np.empty_like(dA)
        for i in range(nnodes):
            try:
                amp_nodes[i] = samp["spsig_amp_{}".format(i)]
            except ValueError:
                amp_nodes[i] = attrs["spsig_amp_{}".format(i)]
            try:
                lfreqs[i] = samp["spsig_logfreq_{}".format(i)]
            except ValueError:
                lfreqs[i] = attrs["spsig_logfreq_{}".format(i)]

        dphi = CubicSpline(lfreqs, [samp["spsig_phase_{}".format(f)] for f in range(nnodes)])
        da = CubicSpline(lfreqs, amp_nodes)
        sel = (freqs > flow) & (freqs < fhigh)
        dA[sel] = da(np.log(freqs[sel]))
        dPhi[sel] = dphi(np.log(freqs[sel]))
        amps[k, :][sel] = dA[sel]
        phis[k, :][sel] = (2.0 + 1j * dPhi[sel]) / (2.0 - 1j * dPhi[sel])
        sigF[k, :] = (1.0 + amps[k, :]) * phis[k, :]
        logfs[k, :] = lfreqs
        k += 1
    return {"amps": amps, "phis": phis, "sigFactor": sigF, "logfs": logfs, "freqs": freqs}


def animate_splines(
    data,
    file,
    filename=None,
    zoom=False,
    lvls=[90],
    flow=30.0,
    fhigh=2048.0,
    progress=False,
    logger=EmptyLogger(),
    NFRAMES=100,
    avg=None,
):

    fig, axs = plt.subplots(nrows=2, ncols=1, figsize=(12, 10), sharex="all")
    ax = axs[0]
    ax.set_xscale("log")
    ax.set_ylabel(r"$\delta A$  (%)")
    ax = axs[1]
    ax.set_xscale("log")
    if zoom:
        axs[0].set_ylim((-50, 50))
    if zoom:
        axs[1].set_ylim((-90, 90))
    ax.set_xlabel("Frequency (Hz)")
    ax.set_ylabel(r"$\delta \phi$ (Deg)")
    axs[0].set_title("Amplitude")
    axs[1].set_title("Phase")
    pos_len = len(data["amps"][:, 0])
    nnodes = len(data["logfs"][0, :])
    skip = int(pos_len / NFRAMES)
    unwrapped_angles = np.unwrap(np.angle(data["phis"]))
    amps = data["amps"]
    logfs = data["logfs"]
    x = data["freqs"]
    ax.set_xlim(flow, fhigh)
    fig.suptitle("Spline Plot for file: {}".format(file))
    fig.set_size_inches(10, 8)
    cam = Camera(fig)
    if avg is not None:
        indices = range(1 + avg, pos_len, skip)
        if len(indices) < 10:
            logger.info(f"not enough samples to create running average animation with avg = {avg}")
            return
        numpy_func = np.mean
    else:
        indices = range(1, pos_len, skip)
        numpy_func = np.median
    total = len(indices)
    logger.debug(f"Creating {total} frames for gif creation to save at {filename}")
    with progress_bar(progress, total) as prg_bar:
        for i in indices:
            if avg is not None:
                start = int(i - avg)
            else:
                start = 0
            running_med = np.exp(numpy_func(logfs[start:i, :], axis=0))
            running_amp_mean = numpy_func(amps[start:i, :], axis=0)
            running_phase_mean = numpy_func(unwrapped_angles[start:i, :], axis=0)
            ax = axs[0]
            for j in range(nnodes):
                ax.axvline(running_med[j], color="r")
                ax = axs[1]
                ax.axvline(running_med[j], color="r")
                ax = axs[0]
            ax.axhline(0, color="m", ls="--")
            ax.plot(x, running_amp_mean * 100, "k")
            for lvl in lvls:
                tmp = (100 - lvl) / 2.0
                low = np.percentile(amps[start:i, :], tmp, axis=0) * 100
                high = np.percentile(amps[start:i, :], 100 - tmp, axis=0) * 100
                ax.fill_between(x, low, high, color="k", alpha=0.3, lw=0)
                ax = axs[1]
                low = np.percentile(unwrapped_angles[start:i, :], tmp, axis=0) * 180 / np.pi
                high = np.percentile(unwrapped_angles[start:i, :], 100 - tmp, axis=0) * 180 / np.pi
                ax.fill_between(x, low, high, color="k", alpha=0.3, lw=0)
                ax = axs[0]
            ax = axs[1]
            ax.axhline(0, color="m", ls="--")
            ax.plot(x, running_phase_mean * 180 / np.pi, "k")
            cam.snap()
            prg_bar.update(1)
        logger.debug("Animating snapped camera frames")
        animation = cam.animate()
        logger.info("Saving Animation")
        animation.save(filename, writer="pillow")


if __name__ == "__main__":
    ptmcmcFile = "/home/bedelman/SSD2/projects/signal_spline/modGR/PTMCMC.output.70119.h5"
    post = h5py.File(ptmcmcFile, "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
    attrs = post.attrs
    data = generate_spline_factors(post, attrs)
    animate_splines(data, ptmcmcFile, filename="../modGR_spline_animation_test.gif", progress=True, zoom=True)
