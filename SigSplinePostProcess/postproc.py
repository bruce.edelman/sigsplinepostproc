from argparse import ArgumentParser, ArgumentError
from .utils import (
    setup_logger,
    get_posts_from_files,
    check_posterior_params,
    check_yes_no_input,
    create_runscript,
    creat_postproc_script,
)
from .plotting import *
from .signal import SignalDict, DataDump
from .psd import Psd
from .modwaves import ModWaves
from .splines import animate_splines
import os, datetime, sys, subprocess
import pickle
from matplotlib import pyplot as plt

DEFAULT_POST_PARAMS = ["chirpmass", "logdistance", "deltalogl"]


class PipeLine(object):
    def __init__(self):
        self.input_args = self.setup_parser()
        if self.input_args["labels"] is None:
            self.input_args["labels"] = ["file{}".format(i) for i in range(len(self.input_args["posterior_files"]))]
        self.outdir, self.plotdir, self.fddir, self.tddir = self.setup_directories()
        self.logger = self.setup_logger()
        self.logger.info("Logger setup and start at: {}..".format(datetime.datetime.now()))
        self.logger.info("Creating runscript for fast rerunning...")
        self.rerun_path = create_runscript(sys.argv, self.outdir, self.input_args["rerun"])
        self.man_burns, self.labels = self.check_for_man_burn()
        self.logger.info(f"Using manual burnins = {self.man_burns}")
        self.logger.info("Creating postproc script...")
        self.posproc_path = creat_postproc_script(
            self.input_args["posterior_files"],
            self.input_args["labels"],
            self.outdir,
            manburns=self.man_burns,
            run=self.input_args["run_posplots"],
        )
        self.ifos = self.input_args["ifos"]
        self.logger.debug("PostProc Pipeine setup with ifos = {}".format(self.ifos))
        if self.input_args["psds"] is not None:
            self.logger.debug("Creating Psd object with input psd files = {}".format(self.input_args["psds"]))
            self.psd = Psd(self.input_args["psds"], self.input_args["ifos"])
        elif self.input_args["asds"] is not None:
            self.logger.debug("Creating Psd object with input asd files = {}".format(self.input_args["asds"]))
            self.psd = Psd(self.input_args["asds"], self.input_args["ifos"], asd=True)
        else:
            self.psd = None
            self.logger.debug("No input psd/asd files")
        if self.input_args["signals"] is not None:
            self.logger.debug("Creating SignalDict with input signal files = {}".format(self.input_args["signals"]))
            self.signals = SignalDict(self.input_args["signals"], self.input_args["ifos"], self.get_signal_args())
        else:
            self.logger.debug("No Signals files input")
            self.signals = None
        self.modwavefiles = {}
        self.modlabels = {}
        for file, label in zip(self.input_args["posterior_files"], self.labels):
            self.logger.debug(f"Creating modWaves for file = {file}, label = {label}")
            waves = ModWaves(
                file, self.outdir, logger=self.logger, man_burn=self.man_burns[label], label=label, **self.input_args
            )
            if waves.spline:
                self.modwavefiles["spline"] = waves.modwaves_outfile
                self.modlabels["spline"] = waves.label
                self.flow = waves.flow
                self.fhigh = waves.fhigh
                self.srate = waves.srate
                if self.input_args["animate"]:
                    animate_of = self.plotdir + "spline_animation.gif"
                    self.logger.info(f"Creating stable animation of spline plots at: {animate_of}")
                    animate_splines(
                        waves.spline_data,
                        file,
                        filename=animate_of,
                        lvls=[80, 90],
                        flow=self.flow,
                        fhigh=self.fhigh,
                        logger=self.logger,
                        progress=True,
                    )
                    animate_of = self.plotdir + "fast_spline_animation.gif"
                    self.logger.info(f"Creating fast animation of spline plots at: {animate_of}")
                    animate_splines(
                        waves.spline_data,
                        file,
                        filename=animate_of,
                        lvls=[80, 90],
                        flow=self.flow,
                        fhigh=self.fhigh,
                        logger=self.logger,
                        progress=True,
                        avg=150,
                    )
            else:
                self.modwavefiles["nospline"] = waves.modwaves_outfile
                self.modlabels["nospline"] = waves.label
        try:
            nospline_files = self.modwavefiles["nospline"]
        except KeyError:
            nospline_files = None

        if nospline_files is not None:
            files = self.input_args["posterior_files"][::-1]
            print(self.modlabels, self.man_burns)
            man_burn = [self.man_burns[self.modlabels["spline"]], self.man_burns[self.modlabels["nospline"]]]
            pos, ns = get_posts_from_files(files, manburn=man_burn, burn=True)
            check_posterior_params(ns, pos, outdir=self.plotdir, logger=self.logger, show=False, save=True)

        if self.input_args["data_dump"]:
            self.logger.info("Creating DataDump object to make datadump plots...")
            data_dump = DataDump(
                self.input_args["posterior_files"],
                self.input_args["labels"],
                self.ifos,
                flow=self.flow,
                fhigh=self.fhigh,
                logger=self.logger,
            )
            self.logger.info("Creating DataDump plots from object...")
            data_dump.make_plots(self.plotdir)

        self.logger.info("Creating fd amplitude plot ...")
        fd_amp = plot_modified_waveform_amp_fd(
            self.modwavefiles["spline"], filename=self.fddir + "fd_amp.png", ifos=self.ifos
        )
        plt.close(fd_amp)
        self.logger.info("Creating fd phase plot ...")
        fd_phase = plot_modified_waveform_phase_fd(
            self.modwavefiles["spline"], filename=self.fddir + "fd_phase_log.png", log=True, ifos=self.ifos
        )
        plt.close(fd_phase)
        if nospline_files is not None:
            self.logger.info("Creating fd amplitude plot ...")
            fd_amp = plot_modified_waveform_amp_fd(
                self.modwavefiles["spline"],
                filename=self.fddir + "fd_amp_ns.png",
                ifos=self.ifos,
                flow=self.flow,
                fhigh=self.fhigh,
                no_mod_waves=nospline_files,
            )
            plt.close(fd_amp)
            self.logger.info("Creating fd phase plot ...")
            fd_phase = plot_modified_waveform_phase_fd(
                self.modwavefiles["spline"],
                flow=self.flow,
                fhigh=self.fhigh,
                filename=self.fddir + "fd_phase_log_ns.png",
                no_mod_waves=nospline_files,
                log=True,
                ifos=self.ifos,
            )
            plt.close(fd_phase)
            for ifo in self.ifos:
                self.logger.info(f"Creating {ifo} fd amplitude plot with nospline...")
                fd_amp = plot_modified_waveform_amp_fd(
                    self.modwavefiles["spline"],
                    filename=self.fddir + f"fd_amp_{ifo}_nospline.png",
                    no_mod_waves=nospline_files,
                    ifos=[ifo],
                    flow=self.flow,
                    fhigh=self.fhigh,
                )
                plt.close(fd_amp)
                self.logger.info(f"Creating {ifo} fd phase plot with nospline...")
                fd_phase = plot_modified_waveform_phase_fd(
                    self.modwavefiles["spline"],
                    log=True,
                    no_mod_waves=nospline_files,
                    ifos=[ifo],
                    flow=self.flow,
                    fhigh=self.fhigh,
                    filename=self.fddir + f"fd_phase_{ifo}_log_nospline.png",
                )
                plt.close(fd_phase)
                if self.signals is not None:
                    self.logger.info(
                        f"Creating {ifo} fd amplitude plot with signal from file =" f"{self.signals(ifo).file}..."
                    )
                    fd_amp = plot_modified_waveform_amp_fd(
                        nospline_files,
                        filename=self.fddir + f"fd_amp_nospline_{ifo}_signal.png",
                        flow=self.flow,
                        fhigh=self.fhigh,
                        signal=self.signals,
                        ifos=[ifo],
                    )
                    plt.close(fd_amp)
                    self.logger.info(f"Creating {ifo} fd phase plot with signal from file ={self.signals(ifo).file}...")
                    fd_phase = plot_modified_waveform_phase_fd(
                        nospline_files,
                        log=True,
                        signal=self.signals,
                        flow=self.flow,
                        fhigh=self.fhigh,
                        ifos=[ifo],
                        sig_flow=self.flow,
                        filename=self.fddir + f"fd_phase_nospline_{ifo}_log_signal.png",
                    )
                    plt.close(fd_phase)
                else:
                    self.logger.info(f"Creating {ifo} fd amplitude plot with maxl_waveform")
                    fd_amp = plot_modified_waveform_amp_fd(
                        nospline_files,
                        filename=self.fddir + f"fd_amp_nospline_{ifo}_signal.png",
                        flow=self.flow,
                        fhigh=self.fhigh,
                        ifos=[ifo],
                        maxl=True,
                    )
                    plt.close(fd_amp)
                    self.logger.info(f"Creating {ifo} fd phase plot with maxl waveform")
                    fd_phase = plot_modified_waveform_phase_fd(
                        nospline_files,
                        log=True,
                        maxl=True,
                        flow=self.flow,
                        fhigh=self.fhigh,
                        ifos=[ifo],
                        sig_flow=self.flow,
                        filename=self.fddir + f"fd_phase_nospline_{ifo}_log_signal.png",
                    )
                    plt.close(fd_phase)
        if self.signals is not None:
            self.logger.info(f"Creating fd amplitude plot with signals from files =" f"{self.signals.files}...")
            fd_amp = plot_modified_waveform_amp_fd(
                self.modwavefiles["spline"],
                filename=self.fddir + "fd_amp.png",
                ifos=self.ifos,
                signal=self.signals,
                flow=self.flow,
                fhigh=self.fhigh,
            )
            plt.close(fd_amp)
            self.logger.info(f"Creating fd phase plot with signals from files =" f"{self.signals.files}...")
            fd_phase = plot_modified_waveform_phase_fd(
                self.modwavefiles["spline"],
                sig_flow=self.flow,
                filename=self.fddir + "fd_phase_log.png",
                fhigh=self.fhigh,
                log=True,
                ifos=self.ifos,
                signal=self.signals,
                flow=self.flow,
            )
            plt.close(fd_phase)
            for ifo in self.ifos:
                self.logger.info(
                    f"Creating {ifo} fd amplitude plot with signal from file =" f"{self.signals(ifo).file}..."
                )
                fd_amp = plot_modified_waveform_amp_fd(
                    self.modwavefiles["spline"],
                    filename=self.fddir + f"fd_amp_{ifo}_signal.png",
                    signal=self.signals,
                    ifos=[ifo],
                    flow=self.flow,
                    fhigh=self.fhigh,
                )
                plt.close(fd_amp)
                self.logger.info(f"Creating {ifo} fd phase plot with signal from file ={self.signals(ifo).file}...")
                fd_phase = plot_modified_waveform_phase_fd(
                    self.modwavefiles["spline"],
                    log=True,
                    sig_flow=self.flow,
                    signal=self.signals,
                    flow=self.flow,
                    ifos=[ifo],
                    fhigh=self.fhigh,
                    filename=self.fddir + f"fd_phase_{ifo}_log_signal.png",
                )
                plt.close(fd_phase)
        elif self.input_args["maxl"]:
            self.logger.info(f"Creating fd amplitude plot with maxl waveform")
            fd_amp = plot_modified_waveform_amp_fd(
                self.modwavefiles["spline"],
                filename=self.fddir + "fd_amp.png",
                ifos=self.ifos,
                maxl=True,
                flow=self.flow,
                fhigh=self.fhigh,
            )
            plt.close(fd_amp)
            self.logger.info(f"Creating fd phase plot with maxl waveform")
            fd_phase = plot_modified_waveform_phase_fd(
                self.modwavefiles["spline"],
                sig_flow=self.flow,
                filename=self.fddir + "fd_phase_log.png",
                fhigh=self.fhigh,
                log=True,
                ifos=self.ifos,
                maxl=True,
                flow=self.flow,
            )
            plt.close(fd_phase)
        else:
            self.logger.info(f"Creating fd amplitude plot without maxl waveform")
            fd_amp = plot_modified_waveform_amp_fd(
                self.modwavefiles["spline"],
                filename=self.fddir + "fd_amp.png",
                ifos=self.ifos,
                flow=self.flow,
                fhigh=self.fhigh,
            )
            plt.close(fd_amp)
            self.logger.info(f"Creating fd phase plot without maxl waveform")
            fd_phase = plot_modified_waveform_phase_fd(
                self.modwavefiles["spline"],
                sig_flow=self.flow,
                filename=self.fddir + "fd_phase_log.png",
                fhigh=self.fhigh,
                log=True,
                ifos=self.ifos,
                flow=self.flow,
            )
            plt.close(fd_phase)
        if self.psd is not None:
            spectral_noise_plot = self.psd.plot(filename=self.fddir + "psds.png", lowf=self.flow, highf=self.fhigh)
            plt.close(spectral_noise_plot)
            for ifo in self.ifos:
                self.logger.info(f"Creating {ifo} fd amplitude plot with psds...")
                fd_amp = plot_modified_waveform_amp_fd(
                    self.modwavefiles["spline"],
                    no_mod_waves=nospline_files,
                    ifos=[ifo],
                    filename=self.fddir + f"fd_amp_{ifo}_psd.png",
                    psd=self.psd,
                    flow=self.flow,
                    fhigh=self.fhigh,
                )
                plt.close(fd_amp)
                self.logger.info(f"Creating {ifo} fd phase plot with psds...")
                fd_phase = plot_modified_waveform_phase_fd(
                    self.modwavefiles["spline"],
                    log=True,
                    no_mod_waves=nospline_files,
                    flow=self.flow,
                    fhigh=self.fhigh,
                    ifos=[ifo],
                    filename=self.fddir + f"fd_phase_{ifo}_log_psd.png",
                    psd=self.psd,
                )
                plt.close(fd_phase)

        if self.tddir is not None:
            self.logger.info("Creating td amplitude plot ...")
            td_amp = plot_modified_waveform_amp_td(
                self.modwavefiles["spline"], filename=self.tddir + "td_amp.png", ifos=self.ifos
            )
            plt.close(td_amp)
            self.logger.info("Creating td phase plot ...")
            td_phase = plot_modified_waveform_phase_td(
                self.modwavefiles["spline"], filename=self.tddir + "td_phase.png", ifos=self.ifos
            )
            plt.close(td_phase)
            self.logger.info("Creating zoomed td amplitude plot ...")
            td_amp = plot_modified_waveform_amp_td(
                self.modwavefiles["spline"], filename=self.tddir + "td_amp_zoom.png", ifos=self.ifos, zoom=True
            )
            plt.close(td_amp)
            self.logger.info("Creating zoomed td phase plot ...")
            td_phase = plot_modified_waveform_phase_td(
                self.modwavefiles["spline"], zoom=True, filename=self.tddir + "td_phase_zoom.png", ifos=self.ifos
            )
            plt.close(td_phase)

            if nospline_files is not None:
                for ifo in self.ifos:
                    self.logger.info(f"Creating {ifo} td amplitude plot with nospline...")
                    td_amp = plot_modified_waveform_amp_td(
                        self.modwavefiles["spline"],
                        filename=self.tddir + f"td_amp_{ifo}_nospline.png",
                        no_mod_waves=nospline_files,
                        ifos=[ifo],
                    )
                    plt.close(td_amp)
                    self.logger.info(f"Creating {ifo} td phase plot with nospline...")
                    td_phase = plot_modified_waveform_phase_td(
                        self.modwavefiles["spline"],
                        no_mod_waves=nospline_files,
                        ifos=[ifo],
                        filename=self.tddir + f"td_phase_{ifo}_nospline.png",
                    )
                    plt.close(td_phase)
                    self.logger.info(f"Creating zoomed {ifo} td amplitude plot with nospline...")
                    td_amp = plot_modified_waveform_amp_td(
                        self.modwavefiles["spline"],
                        filename=self.tddir + f"td_amp_{ifo}_zoom_nospline.png",
                        no_mod_waves=nospline_files,
                        ifos=[ifo],
                        zoom=True,
                    )
                    plt.close(td_amp)
                    self.logger.info(f"Creating zoomed {ifo} td phase plot with nospline...")
                    td_phase = plot_modified_waveform_phase_td(
                        self.modwavefiles["spline"],
                        no_mod_waves=nospline_files,
                        ifos=[ifo],
                        filename=self.tddir + f"td_phase_{ifo}_zoom_nospline.png",
                        zoom=True,
                    )
                    plt.close(td_phase)

        self.logger.info("Creating spline plot ...")
        spline_plot = plot_splines(
            self.modwavefiles["spline"],
            filename=self.plotdir + "splines.png",
            show=True if not self.input_args["no_input"] else False,
            frange=(self.flow, self.fhigh),
            ifos=self.ifos,
        )
        if not self.input_args["no_input"]:
            zoom = input("Do you want to save a zoomed version? (yes/no)")
            if check_yes_no_input(zoom):
                amp_low, amp_high = [
                    int(x) for x in input("What bounds do you want for the amp spline plot? (low, high)").split()
                ]
                phase_low, phase_high = [
                    int(x) for x in input("What bounds do you want for the phase spline plot? (low, high)").split()
                ]
                zoom_dict = {"amp_low": amp_low, "amp_high": amp_high, "phase_low": phase_low, "phase_high": phase_high}
                self.logger.info("Creating zoomed in spline plot ...")
                spline_plot = plot_splines(
                    self.modwavefiles["spline"],
                    zoom=zoom_dict,
                    filename=self.plotdir + "splines_zoomed.png",
                    frange=(self.flow, self.fhigh),
                    ifos=self.ifos,
                )

        plt.close(spline_plot)
        if self.input_args["animate"]:
            self.logger.debug("Animating stable Frequency Domain Waveform Reconstructions")
            animate_reconstruct(
                self.modwavefiles["spline"],
                filename=self.plotdir + "reconstruct_animation.gif",
                frange=(self.flow, self.fhigh),
                ifos=self.ifos,
                logger=self.logger,
                progress=True,
                srate=self.srate,
            )
            self.logger.debug("Animating fast Frequency Domain Waveform Reconstructions")
            animate_reconstruct(
                self.modwavefiles["spline"],
                filename=self.plotdir + "fast_reconstruct_animation.gif",
                frange=(self.flow, self.fhigh),
                ifos=self.ifos,
                logger=self.logger,
                progress=True,
                srate=self.srate,
                avg=150,
            )
        if not self.input_args["store_h5"]:
            self.logger.info("Cleaning up and removing large h5 mod waves files...")
            os.remove(self.modwavefiles["spline"])
            if nospline_files is not None:
                os.remove(nospline_files)

        if self.input_args["run_posplots"]:
            self.logger.info("running cbcBayesPostProc script ...")
            subprocess.call([self.posproc_path])
            pp_dirs = [self.outdir + f"posplots_{label}" for label in self.input_args["labels"]]
            self.logger.info(f"PostProc Complete in directories = {pp_dirs}")
        self.logger.info("SpSigPostProcess COMPLETE: Saved in directoty {}".format(self.outdir))

    def setup_parser(self):
        parser = ArgumentParser(description="Pipeline to handle post-processing routine of signal spline model PE runs")
        parser.add_argument(
            "-p",
            "--posterior-files",
            nargs="*",
            type=str,
            required=True,
            help="Posterior files from " "LALInference Runs. These need to be in order of labels passed if passed.",
        )
        parser.add_argument(
            "-o",
            "--outdirectory",
            type=str,
            required=True,
            help="outdirectory where to store files." "can be either a relative or absolute path.",
        )
        parser.add_argument(
            "-i",
            "--ifos",
            nargs="*",
            type=str,
            required=True,
            help="list of normal ifo names in order" "that we pass other ifo-specific files",
        )
        parser.add_argument(
            "-l",
            "--labels",
            nargs="*",
            type=str,
            required=False,
            default=None,
            help="label names for " "posterior files. same order as files passed in.",
        )
        parser.add_argument(
            "--psds",
            nargs="*",
            type=str,
            required=False,
            default=None,
            help="psds files passed in " "with same oreder as ifos",
        )
        parser.add_argument(
            "--asds",
            nargs="*",
            type=str,
            required=False,
            default=None,
            help="asds files passed in " "with same oreder as ifos",
        )
        parser.add_argument(
            "--signals",
            nargs="*",
            type=str,
            required=False,
            default=None,
            help="signal files (.gwf) " "passed in with same oreder as ifos",
        )
        parser.add_argument(
            "-d",
            "--debug",
            default=False,
            action="store_true",
            help="boolean flag to make logger in " "debug mode (FALSE)",
        )
        parser.add_argument(
            "--no-reconstruct",
            default=False,
            action="store_true",
            help="boolean flag to turn off " "waveform reconstructions (FALSE)",
        )
        parser.add_argument(
            "--time-domain",
            default=False,
            action="store_true",
            help="boolean flag to turn on time" "domain reconstructions (FALSE)",
        )
        parser.add_argument(
            "--store-h5",
            default=False,
            action="store_true",
            help="boolean flag to keep intermediary h5 files for later use (FALSE)",
        )
        parser.add_argument(
            "--rerun",
            default=False,
            action="store_true",
            help="boolean flag signifiying this was ran " "from the rerun script (FALSE)",
        )
        parser.add_argument(
            "--data-dump",
            default=False,
            action="store_true",
            help="boolean flag to plot data-dump " "files must start with same prefix as posterior files",
        )
        parser.add_argument(
            "--animate",
            default=False,
            action="store_true",
            help="Boolean flag to animate the spline " "plotting or not",
        )
        parser.add_argument(
            "--no-input",
            default=False,
            action="store_true",
            help="Boolean flag to not ask for input " "during postprocessing pipeline",
        )
        parser.add_argument(
            "--event",
            default=None,
            type=str,
            help="If running on an event, pass in the event name to " "get the best values form catalog.json",
        )
        parser.add_argument(
            "--run-posplots",
            default=False,
            action="store_true",
            help="Boolean flag to run the " "cbcBayesPostProc script after" "pipeline completion",
        )
        parser.add_argument(
            "--maxl", default=False, action="store_true", help="Boolean flag to plot max-logl " "waveforms"
        )

        args = vars(parser.parse_args())
        self.check_input(args)
        return args

    def check_for_man_burn(self):
        manburn_file = self.outdir + ".manual_burnins.pkl"
        if self.input_args["signals"] is not None:
            truth = True
        else:
            truth = False
        if not self.input_args["no_input"]:
            if os.path.isfile(manburn_file):
                self.logger.debug(f"found manburn file at {manburn_file}")
                manual_burnins = pickle.load(open(manburn_file, "rb"))
                self.logger.debug(f"Usng manual burnins from file: {manual_burnins}")
            else:
                manual_burnins, labels = self.compare_posteriors(
                    filename=self.plotdir + "posterior_check_noburnin.png", show=True, labels=self.input_args["labels"]
                )
                pickle.dump(manual_burnins, open(manburn_file, "wb"))
            _, labels = self.compare_posteriors(
                filename=self.plotdir + "posterior_check_with_burnin.png",
                show=False,
                labels=self.input_args["labels"],
                ask=False,
                manburn=list(manual_burnins.values()),
            )
            """
            corner_plot = plot_corners(self.input_args['posterior_files'], self.input_args['labels'], manburn=list(manual_burnins.values()),
                                       filename=self.plotdir+'corner_plot.png', truth=truth)
            plt.close(corner_plot)
            """
        else:
            _, labels = self.compare_posteriors(
                filename=self.plotdir + "posterior_check_with_burnin.png",
                show=False,
                labels=self.input_args["labels"],
                ask=False,
            )
            corner_plot = plot_corners(
                self.input_args["posterior_files"],
                self.input_args["labels"],
                filename=self.plotdir + "corner_plot.png",
                truth=truth,
            )
            plt.close(corner_plot)
            if os.path.isfile(manburn_file):
                self.logger.debug(f"found manburn file at {manburn_file}")
                manual_burnins = pickle.load(open(manburn_file, "rb"))
            else:
                self.logger.debug(f"did not find manburn file at {manburn_file} \nSetting manual burnins to 0")
                manual_burnins = {}
                for lab in labels:
                    manual_burnins[lab] = 0
        return manual_burnins, labels

    def compare_posteriors(self, filename=None, show=False, labels=None, ask=True, manburn=None):
        files = self.input_args["posterior_files"]
        posplot = plot_multiple_density(
            files, labels, DEFAULT_POST_PARAMS, filename=filename, show=show, man_burn=manburn
        )
        burns = {}
        if ask:
            for name in labels:
                burns[name] = input(
                    "How many samples do you want to manualy remove from posterior file = {}: ".format(name)
                )
        plt.close(posplot)
        return burns, labels

    def setup_logger(self):
        logger_directory = self.outdir + "logs/"
        logger_lvl = "DEBUG" if self.input_args["debug"] else "INFO"
        run_time = datetime.datetime.now()
        label = f"spsig_postproc_pipeline_{run_time}"
        return setup_logger(logger_directory, label, logger_lvl)

    def setup_directories(self):
        outdir = self.input_args["outdirectory"]
        plot_path = outdir + "plots/"
        fd_path = plot_path + "fd/"
        logger_path = outdir + "logs/"
        if self.input_args["time_domain"]:
            td_path = plot_path + "td/"
        else:
            td_path = None
        paths = [outdir, plot_path, fd_path, logger_path, td_path]
        for path in paths:
            if path is not None:
                if not os.path.exists(path):
                    os.mkdir(path)
        return outdir, plot_path, fd_path, td_path

    @staticmethod
    def check_input(args):
        if args["labels"] is not None and len(args["posterior_files"]) != len(args["labels"]):
            raise ArgumentError(
                "Number of labels for post files must be equal and in same order as " "post files passed in:"
            )
        if args["psds"] is not None and len(args["ifos"]) != len(args["psds"]):
            raise ArgumentError("Number of psd files must be equal and in same order as ifos passed in:")
        elif args["asds"] is not None and len(args["ifos"]) != len(args["asds"]):
            raise ArgumentError("Number of asd files must be equal and in same order as ifos passed in:")
        elif args["signals"] is not None and len(args["ifos"]) != len(args["signals"]):
            if args["signals"][0] != "Mod" and args["signals"][0] != "Pure":
                raise ArgumentError("Number of signal files must be equal and in same order as ifos passed in:")

    def get_signal_args(self):
        DEFAULT_ARGS = {"end_time": 32, "start_time": 0, "channel": "_modGR"}
        args = []
        ifos = self.input_args["ifos"]
        for ifo in ifos:
            ifo_args = DEFAULT_ARGS.copy()
            ifo_args["channel"] = ifo + ifo_args["channel"]
            args.append(ifo_args)
        return args
