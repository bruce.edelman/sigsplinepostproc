import pickle

with open(
    "/home/bedelman/Desktop/prod_results/comparison/DIC_values_GW150914_GW151012_GW151226_GW170608_GW170809_GW170104_GW170729_GW170814_GW170818_GW170823_GW170729_HOM_GW170729_cal.pkl",
    "rb",
) as f:
    dic = pickle.load(f)
for key in dic.keys():
    print(key, "DIC - ", dic[key][1][0] - dic[key][0][0])
    print(key, "WAIC - ", dic[key][1][1] - dic[key][0][1])
