import pylab
import numpy as np
import sys, os
from pycbc import noise, psd, frame, conversions
from pycbc.waveform.generator import FDomainCBCGenerator, TDomainCBCGenerator, FDomainDetFrameGenerator
from pycbc.types import TimeSeries
from scipy.signal import tukey
from pycbc.waveform.utils import phase_from_frequencyseries
import matplotlib.pyplot as plt
import lal as _lal


def window(ts, seglen, a=0.4):
    window = tukey(len(ts), alpha=a / seglen)
    return ts * window


def time_sens_ifft(fs, times):
    nout = (fs.size - 1) * 2
    dift = np.fft.irfft(fs.value * nout) / 2
    new = TimeSeries(dift, epoch=fs.epoch, times=times)
    return new


def generate_Frames(whichmodel, corrections=False, noise_only=False, plots=False, TD=False, signal_only=False):
    # Specify observation time, sampling rate and lower frequency cut-off
    tobs = 8 * 8 * 2 * 2
    samprate = 4096.0 * 2
    f_low = 10.0
    approximant = "IMRPhenomD"
    epoch = 1126259462
    # Get waveforms for H1 and L1. We assume that the corrections are *the same* for H1 and L1. This is consistent with the
    # models for Lorentz-violating GR and simple scalarization, but is not necessarily appropriate for the extreme
    # scalarization example. This is also the assumption being made in Francisco's code.
    generator = FDomainDetFrameGenerator(
        FDomainCBCGenerator,
        0,
        variable_args=["mass1", "mass2", "spin1z", "spin2z", "tc", "ra", "dec", "polarization", "distance"],
        detectors=["H1", "L1"],
        delta_f=1.0 / tobs,
        f_lower=f_low,
        approximant=approximant,
    )

    if TD:
        approximant = "SEOBNRv4"
        generator = FDomainDetFrameGenerator(
            TDomainCBCGenerator,
            epoch,
            variable_args=["mass1", "mass2", "spin1z", "spin2z", "tc", "ra", "dec", "polarization", "distance"],
            detectors=["H1", "L1"],
            delta_f=1.0 / tobs,
            f_lower=f_low,
            approximant=approximant,
        )
    # Specify waveform parameter
    mass1 = 1.35 * 3.0
    mass2 = 1.35
    spin1z = 0.0
    spin2z = 0.0
    tc = 228
    ra = 1.37
    dec = -1.26
    polarization = 2.76
    dist = 100
    dtilde = generator.generate(
        mass1=mass1,
        mass2=mass2,
        spin1z=spin1z,
        spin2z=spin2z,
        tc=tc,
        ra=ra,
        dec=dec,
        polarization=polarization,
        distance=dist,
    )
    dmodtilde = generator.generate(
        mass1=mass1,
        mass2=mass2,
        spin1z=spin1z,
        spin2z=spin2z,
        tc=tc,
        ra=ra,
        dec=dec,
        polarization=polarization,
        distance=dist,
    )
    # Preliminaries - define sampling rate, and reference frequency at which perturbed phase is set to zero
    refreq = 100.0

    tlen = int(samprate / dtilde["H1"].delta_f)
    print(len(dtilde["H1"]))
    print(tlen / 2.0 + 1)
    dtilde["H1"].resize(int(tlen / 2 + 1))
    dtilde["L1"].resize(int(tlen / 2 + 1))
    dmodtilde["H1"].resize(int(tlen / 2 + 1))
    dmodtilde["L1"].resize(int(tlen / 2 + 1))

    # Choose which model to use, define parameters and compute phase/amplitude shifts, Models are 1)
    # Lorentz violating gravity; 2) Sampson et al. spontaneous scalarization; 3) Farr "extreme scalarization" toy model.
    # For now we define the correction to the waveform in H1 and take that in L1 to be the same.
    if whichmodel == 1:
        # Lorentz violating gravity is characterized by three parameters - a power, alpha, plus two amplitudes which are
        # related to the mass of the graviton and the Lorentz-violating correction. We'll label these beta1 and beta2
        alpha = 1.0
        beta1 = 100.0
        beta2 = 100.0
        if alpha == 1.0:
            deltapsi = -1.0 * beta1 * ((dmodtilde["H1"].sample_frequencies) ** (-1.0)) + beta2 * np.log(
                dmodtilde["H1"].sample_frequencies
            )
            dpsiref = -1.0 * beta1 * ((refreq) ** (-1.0)) + beta2 * np.log(refreq)
        else:
            deltapsi = -1.0 * beta1 * ((dmodtilde["H1"].sample_frequencies) ** (-1.0)) - beta2 * (
                (dmodtilde["H1"].sample_frequencies) ** (alpha - 1.0)
            )
            dpsiref = -1.0 * beta1 * ((refreq) ** (-1.0)) - beta2 * (refreq ** (alpha - 1.0))
        deltapsi[deltapsi == np.inf] = 0.0
        deltapsi = deltapsi - dpsiref
        amp = np.ones_like(dmodtilde["H1"].sample_frequencies)
        correction = (1 + amp) * (2.0 + 1j * deltapsi) / (2.0 - 1j * deltapsi)

    elif whichmodel == 2:
        # The Sampson et al. spontaneous scalarization model has two free parameters, which we can take to be the
        # coefficients of the -0.5 and -1.0PN terms
        beta1 = 0.00001
        beta2 = 0.00001
        deltapsi = -beta1 * (np.exp((7.0 / 3.0) * np.log(dmodtilde["H1"].sample_frequencies))) - beta2 * (
            np.exp(2.0 * np.log(dmodtilde["H1"].sample_frequencies))
        )
        dpsiref = -beta1 * (refreq ** (7.0 / 3.0)) - beta2 * (refreq ** (2.0))
        deltapsi = deltapsi - dpsiref
        amp = np.ones_like(dmodtilde["H1"].sample_frequencies)
        correction = (1 + amp) * (2.0 + 1j * deltapsi) / (2.0 - 1j * deltapsi)

    elif whichmodel == 3:
        # In the Farr extreme sclarization model we add a sizeable phase shift over a small number of frequency bins,
        # while simultaneously decreasing the waveform amplitude. We characterie the model with five parameters - the size
        # of the phase shift (dphase), the central frequency at which the phase shift occurs (fz), the width of the interval
        # over which the phase shift occurs (deltaf), the fractional size of the amplitude shift (damp) and the offset in
        # frequency between the phase and amplitude shifts (dfz).
        # dphase=20.0*np.pi
        dphase = 1.047
        fz = 102.0
        deltaf = 1.0
        damp = 0.1
        dfz = 0.0
        deltapsi = 0.5 * np.tanh((dmodtilde["H1"].sample_frequencies - fz) / deltaf)
        deltapsi = deltapsi - 0.5 * np.tanh((refreq - fz) / deltaf)
        deltapsi = dphase * deltapsi
        dlogamp = (
            0.5
            * (np.tanh((dmodtilde["H1"].sample_frequencies - fz - dfz) / deltaf))
            * (np.tanh((dmodtilde["H1"].sample_frequencies - fz - dfz) / deltaf))
        )
        dlogamp = dlogamp - 0.5 * np.tanh((refreq - fz) / deltaf) * np.tanh((refreq - fz) / deltaf)
        dlogamp = damp * dlogamp
        amp = np.exp(dlogamp)
        correction = (1 + amp) * (2.0 + 1j * deltapsi) / (2.0 - 1j * deltapsi)
    elif whichmodel == 4:
        f_ro = 500
        a = 0.001
        sel = dmodtilde["H1"].sample_frequencies >= f_ro
        correction = np.ones(len(dmodtilde["H1"].sample_frequencies))
        correction[sel] = np.exp(-a * (dmodtilde["H1"].sample_frequencies[sel] - f_ro))
        # sel = correction < .001
        # correction[sel] = 0
    else:
        print("Invalid model choice, exiting")
        sys.exit()
    if corrections:
        return correction, dmodtilde["H1"].sample_frequencies, amp, deltapsi

    dmodtilde["H1"]._data = dmodtilde["H1"]._data * correction
    dmodtilde["L1"]._data = dmodtilde["L1"]._data * correction

    # Sanity check - plot in time domain
    sh = dtilde["H1"].to_timeseries(delta_t=1.0 / samprate)
    shmod = dmodtilde["H1"].to_timeseries(delta_t=1.0 / samprate)
    sl = dtilde["L1"].to_timeseries(delta_t=1.0 / samprate)
    slmod = dmodtilde["L1"].to_timeseries(delta_t=1.0 / samprate)

    # Add noise to time series
    delta_f = 1.0 / tobs  #
    flen = int(0.5 * samprate / delta_f) + 1
    f_low = 10.0
    hpsd = psd.from_txt("lalsim_big_psd.txt", flen, delta_f, f_low, is_asd_file=False)
    lpsd = psd.from_txt("lalsim_big_psd.txt", flen, delta_f, f_low, is_asd_file=False)
    tlen = len(sh)
    np.savetxt("scratch/out_psd.txt", np.column_stack([hpsd.sample_frequencies, hpsd.data]))
    print(len(sh))
    print(sh.sample_rate)

    noiseh = noise.noise_from_psd(tlen, 1.0 / sh.sample_rate, hpsd, seed=127)
    noisel = noise.noise_from_psd(tlen, 1.0 / sl.sample_rate, lpsd, seed=137)
    # sh.start_time = noiseh.start_time
    # sl.start_time = noisel.start_time
    noiseh.start_time = sh.start_time
    noisel.start_time = sl.start_time

    noisehmod = noise.noise_from_psd(tlen, 1.0 / shmod.sample_rate, hpsd, seed=127)
    noiselmod = noise.noise_from_psd(tlen, 1.0 / slmod.sample_rate, lpsd, seed=137)

    # shmod.start_time = noisehmod.start_time
    # slmod.start_time = noiselmod.start_time
    noisehmod.start_time = shmod.start_time
    noiselmod.start_time = slmod.start_time

    print(noiseh, noisehmod)
    print(np.count_nonzero(noiseh - noisehmod))
    models = {1: "Lorentz", 2: "Sampson", 3: "Farr", 4: "f_rolloff"}
    aprox_path = f"scratch/flow_10/{approximant}"
    if not os.path.exists(aprox_path):
        os.mkdir(aprox_path)
    print(f"min time = {sh.sample_times[0]}\n max time = {sh.sample_times[-1]}")
    print(shmod.start_time)
    print(shmod.end_time)
    print(shmod.duration)
    epoch = 0
    if noise_only:
        frame.write_frame(
            f"scratch/flow_10/{approximant}/H-H1_PureGR_NSBH_NoiseOnly_FD-{epoch}-{epoch+tobs}.gwf", "H1_modGR", noiseh
        )
        frame.write_frame(
            f"scratch/flow_10/{approximant}/L-L1_PureGR_NSBH_NoiseOnly_FD-{epoch}-{epoch+tobs}.gwf", "L1_modGR", noisel
        )
    if signal_only:
        frame.write_frame(
            f"scratch/flow_10/{approximant}/H-H1_PureGR_NSBH_SignalOnly_FD-{epoch}-{epoch+tobs}.gwf", "H1_modGR", sh
        )
        frame.write_frame(
            f"scratch/flow_10/{approximant}/L-L1_PureGR_NSBH_SignalOnly_FD-{epoch}-{epoch+tobs}.gwf", "L1_modGR", sl
        )
        frame.write_frame(
            f"scratch/flow_10/{approximant}/H-H1_ModGR_{models[whichmodel]}_SignalOnly_FD-{epoch}-{epoch+tobs}.gwf",
            "H1_modGR",
            shmod,
        )
        frame.write_frame(
            f"scratch/flow_10/{approximant}/L-L1_ModGR_{models[whichmodel]}_SignalOnly_FD-{epoch}-{epoch+tobs}.gwf",
            "L1_modGR",
            slmod,
        )

    frame.write_frame(
        f"scratch/flow_10/{approximant}/H-H1_PureGR_NSBH_FD-{epoch}-{epoch+tobs}.gwf", "H1_modGR", noiseh + sh
    )
    frame.write_frame(
        f"scratch/flow_10/{approximant}/L-L1_PureGR_NSBH_FD-{epoch}-{epoch+tobs}.gwf", "L1_modGR", noisel + sl
    )
    frame.write_frame(
        f"scratch/flow_10/{approximant}/H-H1_ModGR_{models[whichmodel]}_FD-{epoch}-{epoch+tobs}.gwf",
        "H1_modGR",
        noisehmod + shmod,
    )
    frame.write_frame(
        f"scratch/flow_10/{approximant}/L-L1_ModGR_{models[whichmodel]}_FD-{epoch}-{epoch+tobs}.gwf",
        "L1_modGR",
        noiselmod + slmod,
    )


def plot_corrections_all_models(show=False):
    models = {1: "Lorentz", 2: "Sampson", 3: "Farr"}
    fig, axs = plt.subplots(nrows=2, ncols=1, sharex="all", figsize=(10, 8))
    for i in range(1, 4):
        correction, freqs, da, dp = generate_Frames(i, corrections=True)
        axs[0].semilogx(freqs, da, label="Model {}".format(models[i]))
        axs[0].set_xlabel("Frequency (Hz)")
        axs[0].set_ylabel("dAmp")
        axs[1].semilogx(freqs, dp, label="Model {}".format(models[i]))
        axs[1].set_ylabel("dPhi")
    axs[0].set_title("Amplitude Correction")
    axs[1].set_title("Phase Correction")
    axs[0].legend()
    axs[1].legend()
    plt.savefig("SimulatedDeviations.png")
    if show:
        plt.show()


if __name__ == "__main__":
    generate_Frames(4, noise_only=True, signal_only=True)
