from pycbc import psd
from matplotlib import pyplot as plt
import numpy as np

seglen = 128
df = 1.0 / seglen
flow = 10.0
fhigh = 4096.0

length = (fhigh) / df

des_psd = psd.analytical.aLIGOZeroDetHighPower(length, df, flow)
f = np.linspace(0, fhigh, length)

des_psd._data[-1] = des_psd._data[-2]

print(des_psd)

plt.loglog(f, des_psd)
plt.show()

filename = "lalsim_big_psd.txt"
outdata = np.column_stack([f, des_psd])
np.savetxt(filename, outdata)
