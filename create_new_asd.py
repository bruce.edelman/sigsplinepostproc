import numpy as np


def resample(f1, asd1, f2, asd2):
    df1 = f1[1] - f1[0]
    df2 = f2[1] - f2[0]

    df_o = max(df1, df2)
    for asd, df, in zip([asd1, asd2], [f1, f2]):
        skip = df_o / df
        asd = asd[:: int(skip)] if int(skip) != 0 else asd
    return f1, asd1, f2, asd2


new = "/home/bedelman/SSD2/data/GWTC-1_psds/GWTC1_Data/GW170817/H1_GW170817_0-8-ASD.dat"
old = "/home/bedelman/PycharmProjects/sigsplinepostproc/scratch/h1_asd.txt"

old_f, old_asd = np.genfromtxt(old, unpack=True)
new_f, new_asd = np.genfromtxt(new, unpack=True)

flow = old_f[0]
fhigh = old_f[-1]
old_df = old_f[1] - old_f[0]
nf = len(old_f)

new_flow = new_f[0]
new_fhigh = new_f[-1]
f = np.arange(1, nf + 1) * old_df
asd = np.empty_like(f)

new_ct = 0

for i, freq in enumerate(f):
    if freq < new_flow or freq > new_fhigh:
        asd[i] = old_asd[i]
    else:
        asd[i] = new_asd[new_ct]
        new_ct += 1

filename = "new_asd_test.dat"

outdata = np.column_stack([f, asd])
np.savetxt(filename, outdata)
