import h5py
from SigSplinePostProcess.splines import generate_spline_factors, animate_splines

ptmcmcFile = "/home/bedelman/SSD2/projects/signal_spline/new_simulated/pycbc_injections/posterior/lalinference_mcmc_MODGR_test_spsigFD.hdf5"
post = h5py.File(ptmcmcFile, "r")["lalinference"]["lalinference_mcmc"]["posterior_samples"]
attrs = post.attrs
data = generate_spline_factors(post, attrs)
animate_splines(data, ptmcmcFile, filename="../modGR_pycbc_animation_test.gif", progress=True, zoom=True)
