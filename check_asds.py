import numpy as np
import matplotlib.pyplot as plt
from pycbc import psd

new = "/home/bedelman/SSD2/data/GWTC-1_psds/GWTC1_Data/GW170817/H1_GW170817_0-8-ASD.dat"
combined = "/home/bedelman/PycharmProjects/sigsplinepostproc/scratch/aLIGO_asd.txt"
old = "/home/bedelman/PycharmProjects/sigsplinepostproc/scratch/h1_asd.txt"
sim = "/home/bedelman/PycharmProjects/sigsplinepostproc/scratch/lalsim_h1_psd.txt"
delta_f = 1 / 8.0
f_low = 10.0
flen = int(0.5 * 4096.0 / delta_f) + 1

# ycbc = psd.aLIGODesignSensitivityP1200087(flen, delta_f, f_low)

fig = plt.figure(figsize=(10, 8))
for asd, l in zip([old, new, combined, sim], ["old", "new", "pycbc", "sim"]):
    f, d = np.genfromtxt(asd, unpack=True)
    if l == "sim":
        d = d ** (1 / 2.0)
    plt.loglog(f, d, label=l, alpha=0.7)

plt.xlabel("Frequency (Hz)")
plt.ylabel("Amplitude Spectral Density")
plt.legend()
plt.grid()
plt.ylim(1e-24, 1e-18)
plt.show()
