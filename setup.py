from distutils.core import setup

setup(
    name="SigSplinePostProcess",
    version="0.2",
    url="https://git.ligo.org/bruce.edelman/sigsplinepostproc.git",
    packages=["SigSplinePostProcess"],
    license="",
    scripts=[
        "bin/InitspsigRun",
        "bin/genSingle_xml_injection",
        "bin/genFD_xml_injection",
        "bin/CompareSpline",
        "bin/run_pipe",
        "bin/setup_splinerun_from_config",
    ],
    long_description=open("README.md").read(),
)
